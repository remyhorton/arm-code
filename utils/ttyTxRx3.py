#!/usr/bin/env python3
## ttyTxRx3.py - RS232 Tx & Rx script (Python3 version)
## (c) Remy Horton, 2017-2019
##
## SPDX-License-Identifier: BSD-3-Clause

import serial,sys

if len(sys.argv) < 2:
    print("USAGE: {0} <tty> <num replies> [outgoing bytes]".format(
            sys.argv[0]
            )
        )
    sys.exit(1)
cntExpectBytes = int(sys.argv[2])
cntReplyBytes = 0
i2c = serial.Serial(port=sys.argv[1],baudrate=57600)

if len(sys.argv) > 3:
    packet = b''
    for octet in sys.argv[3:]:
        bite = bytearray((int(octet,16),))
        packet += bite
    print("Sending {0} bytes..".format(len(packet)))
    cntReplyBytes = i2c.write(packet)
    print("Sent {0}".format(cntReplyBytes))

if cntExpectBytes > 0:
    print("Replies ({0} expected):".format(cntExpectBytes),end="")
    bites = i2c.read(cntExpectBytes)
    for bite in bites:
        print(" {0:x}".format(bite),end="")
    print()
print()

