#!/usr/bin/env python3
## ttyI2C.py -  (Python3 version)
## (c) Remy Horton, 2017-2019
##
## SPDX-License-Identifier: BSD-3-Clause

import serial,sys

if len(sys.argv) < 2:
    print("USAGE: {0} <tty> <addr> [outgoing bytes]".format(sys.argv[0]))
    sys.exit(1)
try:
    addrI2C = int(sys.argv[2],16)
except ValueError:
    print("Error: Invalid I2C address")
    sys.exit(1)

i2c = serial.Serial(port=sys.argv[1],baudrate=115200)

packet = b''
for octet in sys.argv[2:]:
    bite = bytearray((int(octet,16),))
    packet += bite
print("Sending {0} bytes..".format(len(packet)))
cntSent = i2c.write(packet)

bites = i2c.read(2)
valResult,valCode = bites
if valResult == 0:
    print("Failed (code {0})".format(valCode))
else:
    if addrI2C & 0x01:
        print("Success ({0} bytes sent, {1} replies): ".format(
                cntSent,valCode
                ),
            end=''
            )
        bites = i2c.read(valCode)
        for bite in bites:
            print('{0:02x}'.format(bite),end=' ')
        print()
    else:
        print("Success ({0} bytes sent, {1} data)".format(cntSent, valCode))
print()

