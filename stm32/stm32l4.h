/*
   stm32l4.h - Bootstrap for STMicro STM32L4 Series Cortex-M4 (header)
   (C) Remy Horton, 2018
   SPDX-License-Identifier: BSD-3-Clause
*/

#define REG(r) (*((volatile uint32_t *)r))

void ARMmain(void);
void ARMtrap(void);
void ARMtimeout(void);
void ARMirq(uint32_t);
