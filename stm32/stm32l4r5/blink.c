/*
   blink.c - GPIO and Timer demo
   (C) Remy Horton, 2018,2024
   SPDX-License-Identifier: BSD-3-Clause
*/
#include <inttypes.h>
#include <stm32l4.h>

#define REG_GPIOA(offset) (*((volatile uint32_t *)(0x48000000+offset)))
#define REG_GPIOB(offset) (*((volatile uint32_t *)(0x48000400+offset)))
#define REG_GPIOC(offset) (*((volatile uint32_t *)(0x48000800+offset)))
#define REG_GPIOD(offset) (*((volatile uint32_t *)(0x48000c00+offset)))
#define REG_GPIOE(offset) (*((volatile uint32_t *)(0x48001000+offset)))
#define REG_GPIOF(offset) (*((volatile uint32_t *)(0x48001400+offset)))
#define REG_GPIOG(offset) (*((volatile uint32_t *)(0x48001800+offset)))
#define REG_GPIOH(offset) (*((volatile uint32_t *)(0x48001c00+offset)))
#define REG_GPIOI(offset) (*((volatile uint32_t *)(0x48002000+offset)))
#define REG_RCC(offset)   (*((volatile uint32_t *)(0x40021000+offset)))
#define REG_TIM2(offset)  (*((volatile uint32_t *)(0x40000000+offset)))
#define REG_UART(offset) (*((volatile uint32_t *)(0x40004800+offset)))
#define REG_EXTI(offset) (*((volatile uint32_t *)(0x40010400+offset)))
#define REG_NVIC_ISER(offset) (*((volatile uint32_t *)(0xe000e100+offset)))
#define REG_I2C4(offset)  (*((volatile uint32_t *)(0x40008400+offset)))


void timerSetup(void)
    {
    REG_RCC(0x58) |= 1;          // Enable TIM2 clocking
    REG_TIM2(0x2c) = 5000;       // ARR = 5sec
    REG_TIM2(0x00) |= 0b11 << 3; // DIR=1 OPM=1
    REG_TIM2(0x28) = 4000;       // 4MHz / 4000 = 1kHz
    REG_TIM2(0x14) |= 1;         // UG=1 (fixes things up)
    REG_TIM2(0x10) &= ~1llu;     // Clear UIF
    }

void timerWait(const uint32_t millis)
    {
    REG_TIM2(0x24) = millis;
    REG_TIM2(0x00) |= 1;
    while( ! (REG_TIM2(0x10) & 1) )
        {
        }
    REG_TIM2(0x10) &= ~1llu;
    }

void gpioSetup(void)
    {
    // Enable GPIO clocking
    REG_RCC(0x4c) |= 1 << 1;

    // Set Port B pins 12-16 to general output
    REG_GPIOB(0x00) &= ~(0b11llu << (12 << 1));
    REG_GPIOB(0x00) &= ~(0b11llu << (13 << 1));
    REG_GPIOB(0x00) &= ~(0b11llu << (14 << 1));
    REG_GPIOB(0x00) &= ~(0b11llu << (15 << 1));
    REG_GPIOB(0x00) |= 0b01llu << (12 << 1);
    REG_GPIOB(0x00) |= 0b01llu << (13 << 1);
    REG_GPIOB(0x00) |= 0b01llu << (14 << 1);
    REG_GPIOB(0x00) |= 0b01llu << (15 << 1);

    // push/pull, not open-drain
    REG_GPIOB(0x04) = 0;
    }

static void inline gpioSet(const uint32_t pin)
    {
    REG_GPIOB(0x18) = 1 << pin;
    }

void gpioClear(const uint32_t pin)
    {
    REG_GPIOB(0x18) = 0x10000 << pin;
    }

void uartSetup(void)
    {
    // Ungate clocks
    REG_RCC(0x58) |= 1 << 18;
    REG_RCC(0x4c) |= 1 << 3;
    // Setup pins for Tx/Rx
    REG_GPIOD(0x00) = 0b1010 << 16;
    REG_GPIOD(0x24) = 0b01110111;
    // Enable RS232 at 115,200
    REG_UART(0x00) = 0b101100;
    REG_UART(0x0c) = 35;
    // Enable interrupts
    REG_EXTI(0x00) |= 1 << 28;
    REG_EXTI(0x04) |= 1 << 28;
    REG_NVIC_ISER(0x04) |= 1 << (39 - 32);
    // FIFO on
    REG_UART(0x00) |= 1 << 29;
    // Enable UART
    REG_UART(0x00) |= 1;
    }

void i2cSetup(void)
    {
    // Enable HSI16 clock
    REG_RCC(0x00) |= 1 << 8;
    // Ungate I2C4 clock and set it to use HSI16
    REG_RCC(0x5c) |= 1 << 1;
    REG_RCC(0x9c) |= 0b10;
    // Setup I2C pins
    REG_GPIOD(0x00) = 0b1010 << 24;
    REG_GPIOD(0x04) |= 0b11 << 12;
    REG_GPIOD(0x24) = 0b01000100 << 16;
    // Use 100kHz standard with 16MHz clock
    REG_I2C4(0x10) = 0x303d5b;
    // Enable I2C
    REG_I2C4(0x00) |= 1;
    }

void i2cSend(const uint32_t addr, const uint8_t *data, const uint32_t length)
    {
    uint32_t lenSent = 0;
    REG_I2C4(0x04) = addr;
    REG_I2C4(0x04) |= length << 16;
    REG_I2C4(0x04) |= 1 << 13; // START
    while( lenSent < length )
        {
        REG_I2C4(0x28) = data[ lenSent++ ];
        while( ! (REG_I2C4(0x18) & 0b1) );
        }
    while( ! (REG_I2C4(0x18) & 0b1) )
        {
        }
    REG_I2C4(0x28) = 0x11; // STOP
    }

void ARMirq(const uint32_t irq)
    {
#if 1
    uint8_t bite = REG_UART(0x24);
    REG_UART(0x28) = bite;
#else
    // Flush read buffer
    REG_RS232(0x18) = 1 << 3;
#endif
    }

void ARMmain(void)
{
const uint8_t data[2] = {0xaa, 0x55};
timerSetup();
gpioSetup();
uartSetup();
i2cSetup();

// Uncomment to send write to PCF8574
// i2cSend(0x4e, data, 1);

while(1)
    {
    gpioSet(12);
    timerWait(100);
    gpioClear(12);
    gpioSet(13);
    timerWait(200);
    gpioClear(13);
    gpioSet(14);
    timerWait(400);
    gpioClear(14);
    gpioSet(15);
    timerWait(800);
    gpioClear(15);
    }
}
