/*
   stm32l4.c - Bootstrap for STMicro STM32L4 Series Cortex-M4
   (C) Remy Horton, 2018
   SPDX-License-Identifier: BSD-3-Clause
*/
#include <inttypes.h>
#include <stm32l4.h>

extern unsigned __dataSrc;
extern unsigned __dataDst;
extern unsigned __dataDstEnd;
extern unsigned __varStackTop;

/****************************************************************************
 ** Application interrupt stubs
 ****************************************************************************/

__attribute__((weak)) void ARMirq(uint32_t value) { }
__attribute__((weak)) void ARMtimeout(void) { }
__attribute__((weak)) void ARMtrap(void) { }


/****************************************************************************
 ** Wrapped interrupt vectors
 ****************************************************************************/

static void interruptReset(void)
{
// Copy data segment to RAM
unsigned *src = &__dataSrc;
unsigned *dst = &__dataDst;
while( dst < &__dataDstEnd )
    *dst++ = *src++;
// Call program entrypoint
ARMmain();
}

static void interruptIRQ(void)
{
// This is Cortex-M specific rather than silicon-specific
unsigned int valIRQ = *((volatile unsigned int *)0xe000ed04);
ARMirq( (valIRQ & 0x1ff) - 16);
}

static void interruptSysTick(void)
{
ARMtimeout();
}

static void interruptSVCall(void)
//static void interruptSysSrvCallSWI(void)
{
// Supervisor call (SVC instruction) - an OS Trap basically
ARMtrap();
}


/****************************************************************************
 ** Unused interrupt vectors
 ****************************************************************************/

static void interruptNotImpl(void)
{
while(1);
}

static void interruptNMI(void)
{
interruptNotImpl();
}

static void interruptHardFault(void)
{
interruptNotImpl();
}

static void interruptMemManage(void)
{
interruptNotImpl();
}

static void interruptBusFault(void)
{
interruptNotImpl();
}

static void interruptUsageFault(void)
{
interruptNotImpl();
}

//static void interruptSysSrvCallSWI(void)
//{
//interruptNotImpl();
//}

static void interruptMonitor(void)
{
interruptNotImpl();
}

static void interruptPendSV(void)
{
interruptNotImpl();
}

static void interruptReserved(void)
{
interruptNotImpl();
}


__attribute__((section(".stack"),used)) unsigned *ptrStackTop = &__varStackTop;

// en.DM00310109 (Reference Manual) S15.3 (P437)
__attribute__((section(".handlers"),used)) void (*vecHandlers[110])() = {
	interruptReset,
	interruptNMI,
	interruptHardFault,
	interruptMemManage,
	interruptBusFault,
	interruptUsageFault,
	interruptReserved, interruptReserved,
	interruptReserved, interruptReserved,
	interruptSVCall,
	interruptMonitor,
	interruptReserved,
	interruptPendSV,
	interruptSysTick,

	interruptIRQ,  /* Window Watchdog */
	interruptIRQ,  /* PVD/PVM1/PVM2/PVM3/PVM4 */
	interruptIRQ,  /* RTC Tamper/TimeStamp */
	interruptIRQ,  /* RTC Wakeup timer */
	interruptIRQ,  /* Flash */
	interruptIRQ,  /* RCC */
	interruptIRQ,  /* EXTI Line0 */
	interruptIRQ,  /* EXTI Line1 */
	interruptIRQ,  /* EXTI Line2 */
	interruptIRQ,  /* EXTI Line3 */
	interruptIRQ,  /* EXTI Line4 */
	interruptIRQ,  /* DMA1 channel 1 */
	interruptIRQ,  /* DMA1 channel 2 */
	interruptIRQ,  /* DMA1 channel 3 */
	interruptIRQ,  /* DMA1 channel 4 */
	interruptIRQ,  /* DMA1 channel 5 */
	interruptIRQ,  /* DMA1 channel 6 */
	interruptIRQ,  /* DMA1 channel 7 */
	interruptIRQ,  /* ADC1 */
	interruptIRQ,  /* CAN1_TX */
	interruptIRQ,  /* CAN1_RX0 */
	interruptIRQ,  /* CAN1_RX1 */
	interruptIRQ,  /* CAN1_SCE */
	interruptIRQ,  /* EXTI Line[9:5] */
	interruptIRQ,  /* TIM1 Break/TIM15 */
	interruptIRQ,  /* TIM1 Update/TIM16 */
	interruptIRQ,  /* TIM1 trigger and commutation/TIM17 */
	interruptIRQ,  /* TIM1 capture/compare */
	interruptIRQ,  /* TIM2 */
	interruptIRQ,  /* TIM3 */
	interruptIRQ,  /* TIM4 */
	interruptIRQ,  /* I2C1 event */
	interruptIRQ,  /* I2C1 error */
	interruptIRQ,  /* I2C2 event */
	interruptIRQ,  /* I2C2 error */
	interruptIRQ,  /* SPI1 */
	interruptIRQ,  /* SPI2 */
	interruptIRQ,  /* USART1 */
	interruptIRQ,  /* USART2 */
	interruptIRQ,  /* USART3 */
	interruptIRQ,  /* EXTI Line[15:10] */
	interruptIRQ,  /* RTC alarms */
	interruptIRQ,  /* DFSDM1_FLT3 */
	interruptIRQ,  /* TIM8 Break */
	interruptIRQ,  /* TIM8 Update */
	interruptIRQ,  /* TIM8 trigger and commutation */
	interruptIRQ,  /* TIM8 capture compare */
	interruptIRQ,  /* Reserved */
	interruptIRQ,  /* FMC */
	interruptIRQ,  /* SDMMC1 */
	interruptIRQ,  /* TIM5 */
	interruptIRQ,  /* SPI3 */
	interruptIRQ,  /* UART4 */
	interruptIRQ,  /* UART5 */
	interruptIRQ,  /* TIM6 and DAC1 underrun */
	interruptIRQ,  /* TIM7 */
	interruptIRQ,  /* DMA2 channel 1 */
	interruptIRQ,  /* DMA2 channel 2 */
	interruptIRQ,  /* DMA2 channel 3 */
	interruptIRQ,  /* DMA2 channel 4 */
	interruptIRQ,  /* DMA2 channel 5 */
	interruptIRQ,  /* DFSDM1_FLT0 */
	interruptIRQ,  /* DFSDM1_FLT1 */
	interruptIRQ,  /* DFSDM1_FLT2 */
	interruptIRQ,  /* COMP1/COMP2 */
	interruptIRQ,  /* LPTIM1 */
	interruptIRQ,  /* LPTIM2 */
	interruptIRQ,  /* OTG_FS */
	interruptIRQ,  /* DMA2 channel 6 */
	interruptIRQ,  /* DMA2 channel 7 */
	interruptIRQ,  /* LPUART1 */
	interruptIRQ,  /* OCTOSPI1 */
	interruptIRQ,  /* I2C3 event */
	interruptIRQ,  /* I2C3 error */
	interruptIRQ,  /* SAI1 */
	interruptIRQ,  /* SAI2 */
	interruptIRQ,  /* OCTOSPI2 */
	interruptIRQ,  /* TSC */
	interruptIRQ,  /* DSI */
	interruptIRQ,  /* AES */
	interruptIRQ,  /* RNG */
	interruptIRQ,  /* Floating point */
	interruptIRQ,  /* HASH and CRS */
	interruptIRQ,  /* I2C4 event */
	interruptIRQ,  /* I2C4 error */
	interruptIRQ,  /* DCMI */
	interruptIRQ,  /* Reserved */
	interruptIRQ,  /* Reserved */
	interruptIRQ,  /* Reserved */
	interruptIRQ,  /* Reserved */
	interruptIRQ,  /* DMA2D */
	interruptIRQ,  /* LTDC */
	interruptIRQ,  /* LTDC error */
	interruptIRQ,  /* GFXMMU error */
	interruptIRQ,  /* DMAMUX Overrun */
};

