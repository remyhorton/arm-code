ARM Firmware
============
Here is firmware that I have written for ARM-based microcontrollers, typically targeting Cortex-M based silicon, and included here as appendices of articles on my [electronics website][remy-elec]. This repository is split into sub-directories based on vendors, and while the code should run on more than just the models listed, I only give the model numbers for units I have personally used.


### **``nxp``** - *NXP Semiconductor*

* `LPC1112` (Cortex-M0)
* `LPC11U24` (Cortex-M0)
* `KE02Z64` (Cortex-M0+)
* `KE04Z8` (Cortex-M0+)


### **``stm32``** - *ST Microelectronics ARM Cortex series*

* `STM32L4R5` (Cortex-M4)


## Licencing
Unless specified otherwise code within this repository is covered by the [BSD 3-clause Licence][bsd-3-clause].


## Contact

`remy.horton` `(at)` `gmail.com`


[remy-elec]: https://www.remy.org.uk/elec.php
[bsd-3-clause]: https://opensource.org/licenses/BSD-3-Clause