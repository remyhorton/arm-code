#!/bin/bash

export BASE=/opt/openocd/v0.10.0
export SCRIPTS=${BASE}/share/openocd/scripts

${BASE}/bin/openocd \
    -f ${SCRIPTS}/interface/ftdi/olimex-arm-usb-tiny-h.cfg \
    -f ${SCRIPTS}/interface/ftdi/olimex-arm-jtag-swd.cfg \
    -f ${SCRIPTS}/target/ke04.cfg
