/*
    i2c-rs232-master.c - RS232-driven I2C master using KE02Z8
    (C) Remy Horton, 2019
    SPDX-License-Identifier: BSD-3-Clause

    Although notionally a reimplementation of my PIC16F1823
    based USB-I2C master, this one does not yet report slave
    nacks to the host computer.
*/
#include <inttypes.h>
#include <ke02z.h>


void clockSetup(void)
{
// Enable external clock via EXTAL pin
REG8(0x40065000) = 0xa4;  // OSCEN=1 OSCOS=0 OSCSTEN=1 RANGE=1
REG8(0x40064000) = 0xa0;  // CLKS=10 IREFS=0 RDIV=100?
REG8(0x40064001) = 0x10;  // BDIV=000 LP=1
REG32(0x4004801c) = 0;    // Force DIVx=0 (x=0,1,2) GOTCHA!

while( (REG8(0x40064004) & 0x1c) != 0x08); // IREFST!=0 & CLKST!=10
}


/****************************************************************************
 * RS232 routines
 ****************************************************************************/

void rs232Setup(void)
    {
    REG(0x4004800c) |= 1 << 20;     // Enable UART0 clock
    REG8(0x4006a000) = 0;           // No interrupts, 1 stop bit
    REG8(0x4006a001) = 9;           // 9=115200 9=57600 104=9600
    REG8(0x4006a002) = 0;           // 2-wire 8-bit no-parity
    REG8(0x4006a003) = 0x0c;        // Enable Tx & Rx
    }

void rs232WriteByte(const uint8_t bite)
    {
    while( (REG8(0x4006a004) & 0x80) == 0 );
    REG8(0x4006a007) = bite;
    }


/****************************************************************************
 * I2C Master routines
 ****************************************************************************/

void i2cSetup(void)
{
REG32(0x4004800c) |= 1 << 17;   // Enable I2C clocking
REG8(0x40066001) = 0x1d;        // Data rate
REG8(0x40066002) |= I2C_EN;     // Enable (Ctrl1)
}

static inline uint8_t i2cStatBit(const uint8_t mask)
{
return REG8(0x40066003) & mask;
}

static inline void i2cClearBit(const uint8_t mask)
{
REG8(0x40066003) = mask;
}

void i2cWrite(uint8_t addr, int reg1, int reg2, int len, uint8_t *data)
{
int pos = 0;

REG8(0x40066002) |= I2C_TX;
REG8(0x40066002) |= I2C_MST;
REG8(0x40066004) = addr & 0xfe;

if(reg1 != -1)
    {
    if(reg2 != -1)
        pos = -2;
    else
        pos = -1;
    }
while(1)
    {
    while(! i2cStatBit(I2C_INTF) )
        {
        }
    i2cClearBit(I2C_INTF);
    if( i2cStatBit(I2C_RXAK) )
        {
        break;
        }
    if(pos == -2)
        REG8(0x40066004) = reg1;
    else if(pos == -1)
        {
        if(reg2 != -1)
            REG8(0x40066004) = reg2;
        else
            REG8(0x40066004) = reg1;
        }
    else if(pos == len)
        {
        break;
        }
    else
        {
        REG8(0x40066004) = data[pos];
        }
    pos++;
    }
REG8(0x40066002) &= ~I2C_MST;
}


void i2cRead(uint8_t addr, int reg1, int reg2, int len, uint8_t *data)
{
int pos = -1;
uint8_t bite;

REG8(0x40066002) |= I2C_TX;
REG8(0x40066002) |= I2C_MST;
REG8(0x40066004) = addr | 0x01;
REG8(0x40066002) &= ~I2C_TXAK;

if(reg1 != -1)
    {
    if(reg2 != -1)
        pos = -3;
    else
        pos = -2;
    }
while(1)
    {
    while(! i2cStatBit(I2C_INTF) )
        {
        }
    i2cClearBit(I2C_INTF);
    if(pos == -3)
        REG8(0x40066004) = reg1;
    else if(pos == -2)
        {
        if(reg2 != -1)
            REG8(0x40066004) = reg2;
        else
            REG8(0x40066004) = reg1;
        }
    else if(pos == -1)
        {
        REG8(0x40066002) &= ~I2C_TX;
        bite = REG8(0x40066004);  // Clear out junk
        }
    else if(pos == len-1)
        {
        REG8(0x40066002) &= ~I2C_MST;
        bite = REG8(0x40066004);
        data[pos] = bite;
        break;
        }
    else
        {
        if(pos == len-2)
            REG8(0x40066002) |= I2C_TXAK;
        bite = REG8(0x40066004);
        data[pos] = bite;
        }
    pos++;
    }
REG8(0x40066002) &= ~I2C_MST;
}


/****************************************************************************
 * I2C Master routines
 ****************************************************************************/

void ARMmain(void)
{
uint8_t  data[256];
uint16_t cntData = 0;
uint8_t  bite;
int reg1;
int reg2;
uint8_t lenData;

// Disable watchdog.
REG16(0x40052002) = 0x20c5;
REG16(0x40052002) = 0x28d9;
REG8(0x40052001) = 0;
REG16(0x40052004) = 0xffff;
REG8(0x40052000) = 0;

clockSetup();
rs232Setup();
i2cSetup();

while(1)
    {
    if( (REG8(0x4006a004) & 0x20) != 0 )
        {
        bite = REG8(0x4006a007);
        data[cntData++] = bite;

        if(cntData < 2)
            continue;
        if(cntData == 2 && data[1] > 2)
            {
            cntData = 0;
            rs232WriteByte(0);
            rs232WriteByte(1);
            continue;
            }
        if(data[1] == 0 && cntData < 3)
            continue;
        if(data[1] == 1 && cntData < 4)
            continue;
        if(data[1] == 2 && cntData < 5)
            continue;
        lenData = data[2+data[1]];
        if(lenData > 250)
            {
            cntData = 0;
            rs232WriteByte(0);
            rs232WriteByte(lenData);
            continue;
            }
        switch(data[1])
            {
            case 0:
                reg1 = -1;
                reg2 = -1;
                break;
            case 1:
                reg1 = data[2];
                reg2 = -1;
                break;
            case 2:
                reg1 = data[1];
                reg2 = data[2];
                break;
            }
        if(data[0] & 0x01)
            {
            i2cRead(data[0], reg1, reg2, lenData, data);
            rs232WriteByte(1);
            rs232WriteByte(lenData);
            for(cntData=0; cntData < lenData; cntData++)
                rs232WriteByte(data[cntData]);
            cntData = 0;
            continue;
            }
        if(3 + data[1] + lenData > cntData)
            continue;

        i2cWrite(data[0], reg1, reg2, lenData, &data[3+data[1]]);
        rs232WriteByte(1);
        rs232WriteByte(lenData);
        cntData = 0;
        }
    }
}

