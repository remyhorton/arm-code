/*
    clocktest.c - Toggle KE04 pin XX with period of 16,000 clock cycles
    (C) Remy Horton, 2019
    SPDX-License-Identifier: BSD-3-Clause
*/
#include <inttypes.h>
#include <ke02z.h>


#define TOGGLED_PINS 0x00000800

void ClockTest(void)
{
//REG8(0x40064001) = 0x00; // 1:1    1.501kHz -> 24.016MHz
//REG8(0x40064001) = 0x20; // 1:2    750.6Hz  -> 12.0096MHz
//REG8(0x40064001) = 0x40; // 1:4    375.4Hz
//REG8(0x40064001) = 0x60; // 1:8
//REG8(0x40064001) = 0x80; // 1:16
//REG8(0x40064001) = 0xa0; // 1:32
//REG8(0x40064001) = 0xc0; // 1:64
//REG8(0x40064001) = 0xe0; // 1:128

// Activate timer clocking
REG(0x4004800c) |= 0x02;
REG_PIT_MCR = 0;
//REG_PIT_LDVAL0 = 8000lu * 1000lu;
REG_PIT_LDVAL0 = 8000lu;
REG_PIT_TCTRL0 = 0x01;

REG(0xf8000014) = TOGGLED_PINS;
while(1)
    {
    REG(0xf8000004) = TOGGLED_PINS;
    while( REG_PIT_TFLG0 == 0);
    REG_PIT_TFLG0 = 1;

    REG(0xf8000008) = TOGGLED_PINS;
    while( REG_PIT_TFLG0 == 0);
    REG_PIT_TFLG0 = 1;
    }
}

void ARMmain(void)
{
// Disable watchdog.
REG16(0x40052002) = 0x20c5;
REG16(0x40052002) = 0x28d9;
REG8(0x40052001) = 0;
REG16(0x40052004) = 0xffff;
REG8(0x40052000) = 0;

// Enable external clock via EXTAL pin
REG8(0x40065000) = 0xa4;  // OSCEN=1 OSCOS=0 OSCSTEN=1 RANGE=1
REG8(0x40064000) = 0xa0;  // CLKS=10 IREFS=0 RDIV=100?
//REG8(0x40064001) = 0x30;  // BDIV=001 LP=1
REG8(0x40064001) = 0x10;  // BDIV=000 LP=1
REG32(0x4004801c) = 0;
while( (REG8(0x40064004) & 0x1c) != 0x08); // IREFST!=0 & CLKST!=10

ClockTest();
}

