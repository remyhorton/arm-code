/*
   lpc111x.c - Bootstrap for NXP LPC111x Cortex-M0
   (C) Remy Horton, 2018
   SPDX-License-Identifier: BSD-3-Clause
*/
#include <inttypes.h>
#include <lpc111x.h>

extern unsigned __nxpBss;
extern unsigned __nxpBssEnd;
extern unsigned __nxpDataSect;
extern unsigned __nxpDataDst;
extern unsigned __nxpDataEnd;
extern unsigned __nxpStackTop;

__attribute__((weak)) void ARMirq(uint32_t value) { }
__attribute__((weak)) void ARMtimeout(void) { }
__attribute__((weak)) void ARMtrap(void) { }


/* All the callbacks are put into section .callbacks which on linking is
   always put before .text (i.e. all other program code), so the checksum
   value NXP_VERTCRC is not affected by code outside of this file */
#define NXP_VECTCRC 0xefffda00

/****************************************************************************
 ** Wrapped interrupt vectors
 ****************************************************************************/


__attribute__((section(".callbacks")))
static void interruptReset(void)
{
// Copy data segment to RAM
unsigned *src = &__nxpDataSect;
unsigned *dst = &__nxpDataDst;
while( dst < &__nxpDataEnd )
    *dst++ = *src++;

// Zero out BSS
dst = &__nxpBss;
while( dst < &__nxpBssEnd )
    *dst++ = 0;

// Call program entrypoint
ARMmain();
}

__attribute__((section(".callbacks")))
static void interruptIRQ(void)
{
unsigned int valIRQ = *((volatile unsigned int *)0xe000ed04);
ARMirq( (valIRQ & 0x3f) - 16);
}

__attribute__((section(".callbacks")))
static void interruptSysTick(void)
{
ARMtimeout();
}

__attribute__((section(".callbacks")))
static void interruptSVCall(void)
{
// Supervisor call (SVC instruction) - an OS Trap basically
ARMtrap();
}


/****************************************************************************
 ** Unused interrupt vectors
 ****************************************************************************/

__attribute__((section(".callbacks")))
static void interruptNotImpl(void)
{
while(1);
}

__attribute__((section(".callbacks")))
static void interruptPendSV(void)
{
interruptNotImpl();
}

__attribute__((section(".callbacks")))
static void interruptNMI(void)
{
interruptNotImpl();
}

__attribute__((section(".callbacks")))
static void interruptHardFault(void)
{
interruptNotImpl();
}

__attribute__((section(".callbacks")))
static void interruptReserved(void)
{
interruptNotImpl();
}


__attribute__((section(".stack"),used)) unsigned *ptrStackTop = &__nxpStackTop;
__attribute__((section(".handlers"),used)) void (*vecHandlers[15+32])() = {
	// Table 428, P467 UM10398
	interruptReset,
	interruptNMI,
	interruptHardFault,
	interruptReserved, interruptReserved, interruptReserved,
	(void*)NXP_VECTCRC, // Checksum
	interruptReserved, interruptReserved, interruptReserved,
	interruptSVCall,
	interruptReserved, interruptReserved,
	interruptPendSV,
	interruptSysTick,
	// Table 55, P70-71
	interruptIRQ,	interruptIRQ,	interruptIRQ,	interruptIRQ,
	interruptIRQ,	interruptIRQ,	interruptIRQ,	interruptIRQ,
	interruptIRQ,	interruptIRQ,	interruptIRQ,	interruptIRQ,
	interruptIRQ,	// 0..12: Deep-sleep wakeup (Sect 3.5.30)
	interruptIRQ,	// C_CAN
	interruptIRQ,	// SPI/SSP1
	interruptIRQ,	// I2C
	interruptIRQ,	// CT16B0
	interruptIRQ,	// CT16B1
	interruptIRQ,	// CT32B0
	interruptIRQ,	// CT32B1
	interruptIRQ,	// SPI/SSP0
	interruptIRQ,	// UART
	interruptIRQ,	// Reserved
	interruptIRQ,	// Reserved
	interruptIRQ,	// ADC
	interruptIRQ,	// Watchdog
	interruptIRQ,	// Brownout Detect
	interruptIRQ,	// Reserved
	interruptIRQ,	// GPIO Port 3
	interruptIRQ,	// GPIO Port 2
	interruptIRQ,	// GPIO Port 1
	interruptIRQ	// GPIO Port 0
};

/*  If this is one of the four patterns in Table 372 in S26.3.8
    then code protection is enabled. Explicitly set it to zero
    to avoid accidentally bricking the chip. */
__attribute__((section(".config"),used)) int32_t _CP = 0x00000000;
