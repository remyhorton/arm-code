/*
    usb.c - USB demo for NXP LPC11U24
    (C) Remy Horton, 2019
    SPDX-License-Identifier: BSD-3-Clause

    Although notionally for the LPC11U24 the features used are available
    on all chips in the LPC11Uxx series. Any QFN-33 variant should suffice
    as a drop-in replacement.

    To connect PuTTY to RS232 port:
        putty -serial /dev/ttyUSB0 -sercfg 115200,8,n,1,N

    For wireshark:
        modprobe usbmon
        !usb.addr == "1.5.3" && !usb.addr == "1.4.3"
*/
#include <inttypes.h>
#include <lpc111x.h>
#include "usb.h"

/*****************************************************************************
 ** Global variables
 *****************************************************************************/

uint8_t  pktSetup[8];
uint32_t newAddr;


/*****************************************************************************
 ** Setup response packets
 *****************************************************************************/

static const char pktDeviceDesc[18] = {
    18,     // Length
    1,      // Type
    0, 2,   // USB version
    0xff,   // Vendor-specified class code
    0xff,   // Sub-class
    0xff,   // Protocol
    64,     // Max packet size
    // The values below are id numbers that should only be used for
    // testing purposes because they are not garanteed to be unique
    // to this firmware. See https://pid.codes/1209/0001/
    0x09,   // idVendor 0x1209
    0x12,
    0x01,   // idProduct 0x0001
    0x00,
    1, 0,   // Device release number
    1,      // Manufacturer
    2,      // Product
    0,      // Serial number (0 = no string)
    1       // Number of configs
};

static const char pktConfigDesc[32] = {
    // Config descriptor
    9,      // Length of this section
    2,      // Type
    32,0,   // Total length of entire set
    1,      // Number interfaces
    1,      // Id of this config
    0,      // No string
    0xc0,   // Self powered; no remote wakeup
    10,     // 20mA max current draw
    // Interface descriptor
    9,      // Length
    4,      // Type
    0,      // Interface ID (Linux needs this to be zero)
    0,      // Alternative (always zero for default)
    2,      // 2 endpoints
    0xff,   // Vendor-defined class
    1,
    1,
    0,      // 0 = No string
    // Endpoint descriptor 1
    7,      // Length
    5,      // Type
    0x01,   // OUT, EP1
    0x03,   // Interrupt
    64, 0,  // Max packet size (2 bytes)
    255,    // Polling interval in 125uS frames
    // Endpoint descriptor 2
    7,      // Length
    5,      // Type
    0x81,   // IN, EP1
    0x03,   // Interrupt
    64, 0,  // Max packet size
    255,    // Polling interval
};

static const char pktStringDesc[6] = {
    6,          // Length
    0x03,       // Type (string),
    0x09, 0x08, // UK English (0x0809)
    0x09, 0x04, // US English (0x0409)
};

static const char *strManu = "https://www.remy.org.uk";
static const char *strProd = "NXP LPC11Uxx USB demo";
static const char *strNone = "<none>";


/*****************************************************************************
 ** RS232 functions.
 ** The UART is used as a debugging side-channel.
 *****************************************************************************/

void setupRS232(void)
{
// Note: Do pin setup before enabling UART clock.
REG(0x40044048) = 0x01;       // PIO0_18: Set to UART Rx
REG(0x4004404c) = 0x01;       // PIO0_19: Set to UART Tx
REG(0x40048080) |= 1 << 12;   // Enable UART clock
REG(0x40048098) = 1;          // UART_PCLK clock divider
REG(0x4000800c) = 0x03;       // 8 data bits, 1 stop bit, no parity
REG(0x4000800c) |= 1 << 7;    // DLAB=1
REG(0x40008000) = 5;          // DLL
REG(0x40008004) = 0;          // DLM
REG(0x4000800c) &= ~(1 << 7); // DLAB=0
REG(0x40008028) = (10 << 4) | 3; // [0:3]=DIVADDVAL=3 [4:7]=MULVAL=10
REG(0x40008008) = 0x07;       // FIFO Enable

uartWriteNL();
uartWriteNL();
uartWriteStr("RS232 Ok");
uartWriteNL();
}

static inline void uartWaitEmpty(void)
{
while( (REG(0x40008014) & (1<<5)) == 0);
}

void uartWriteStr(char *str)
{
while( *str != '\0' )
    {
    uartWaitEmpty();
    REG(0x40008000) = *str;
    str++;
    }
}

void uartWriteChar(const char letter)
{
uartWaitEmpty();
REG(0x40008000) = letter;
}

void uartWriteNL(void)
{
uartWaitEmpty();
REG(0x40008000) = '\r';
uartWaitEmpty();
REG(0x40008000) = '\n';
}

void uartWriteHex(const uint32_t value)
{
static const char letters[] = {
    '0','1','2','3','4',
    '5','6','7','8','9',
    'a','b','c','d','e','f'
    };
uartWaitEmpty();
REG(0x40008000) = letters[ (value >> 4) & 0x0f ];
uartWaitEmpty();
REG(0x40008000) = letters[ value & 0x0f ];
}


/****************************************************************************
 ** Clock setup
 ****************************************************************************/

void setupClock(void)
{
// Bypass system oscillator so that sys_osc_clk is fed by
// the external osccillator directly.
REG_SYSOSCCTRL = 0x01;

// Power up PLL
REG_SYSPLLCTRL = 0x00000022;

// System PLL input
REG_SYSPLLCLKSEL = 1; // 0=irc 1=sysosc
REG_SYSPLLCLKUEN = 0;
REG_SYSPLLCLKUEN = 1;
REG_PDRUNCFG &= ~(1<<7);
while( REG_SYSPLLSTAT == 0 );

// USB PLL input
REG_USB_PLLCLKSEL = 1; // 0=irc 1=sysosc
REG_USB_PLLCLKUEN = 0;
REG_USB_PLLCLKUEN = 1;
REG_USB_PLLCTRL = 0x00000022;
REG_PDRUNCFG &= ~(1<<8);
while( REG_USB_PLLSTAT == 0 );

// Set main clock to PLL Out
REG_MAINCLKSEL = 3; // 0=irc 1=PLLin 2=watchdog 3=PLLout
}


/****************************************************************************
 ** USB functions
 ****************************************************************************/

void resetUSB(void);


void setupUSB(void)
{
REG(0x4004400c) = 1;            // PIO0_3 -> USB_VBUS
REG(0x40044018) = 1;            // PIO0_6 -> USB_CONNECT

REG_SYSAHBCLKCTRL |= (1<<14);   // Clock USB
REG_SYSAHBCLKCTRL |= (1<<27);   // Clock USB SRAM
REG_PDRUNCFG &= ~(1<<10);       // Power up USB Phy

// Both USBPLL and main clock need to be running first
REG_USB_CLKSEL = 0; // Set USB clock 0=USBPLL 1=mainclock
REG_USB_CLKUEN = 0;
REG_USB_CLKUEN = 1;
REG_USB_CLKDIV = 1; // For Low-speed? Not actually supported!!

REG_USB_DEVCMDSTAT &= ~DCS_LPMSUP;
REG(REG_ISER) = 1llu << 22;
REG(REG_ISER) = 1llu << 23;

resetUSB();

REG_USB_DEVCMDSTAT |= DCS_DCON;
}


void printInfo(void)
{
int val2 = REG_USB_INFO;
uartWriteStr("/");
uartWriteHex(val2 & 0x3f);
uartWriteStr("/");
uartWriteHex(val2 >> 11);
uartWriteStr(" ");
}


void resetUSB(void)
{
uint32_t idxEP;

for(idxEP = 0x20004000; idxEP < 0x20004000 + 2048; idxEP += 4)
    REG(idxEP) = 0;

epSetOffset(EP0OUT, 0);
epSetNBytes(EP0OUT, 0, 64);
epSetOffset(EP0OUT, 1);     // SETUP

epSetOffset(EP0IN, 0);
epSetNBytes(EP0IN, 0, 64);

epSetFlags(EP0OUT, 0, EP_ACTIVE);
epSetFlags(EP0OUT, 1, EP_ACTIVE);
epSetFlags(EP0IN,  0, EP_ACTIVE);

for(idxEP=EP1OUT; idxEP<=EP4IN; idxEP++)
    {
    epSetFlags(idxEP, 0, EP_DISABLE);
    epSetFlags(idxEP, 1, EP_DISABLE);
    epSetOffset(idxEP, 0);
    epSetOffset(idxEP, 1);
    epSetNBytes(idxEP, 0, 64);
    epSetNBytes(idxEP, 1, 64);
    }
newAddr = 0;
REG_USB_EPLISTSTART = 0x20004000;
REG_USB_DATABUFSTART = 0x20004000 & 0xffc00000;
REG_USB_EPINUSE = 0;
REG_USB_EPSKIP = 0;
REG_USB_INTSTAT = 0xc00003ff;
REG_USB_INTEN = 0x000003ff | (1<<30) | (1<<31);
REG_USB_DEVCMDSTAT |= DCS_DEVEN;
}


void handleSetup(void)
{
uint8_t *ptrData = epGetData(EP0OUT, 1);
uint8_t *ptrResp;
uint32_t idxByte;
uint32_t cntBytes;

for(idxByte=0; idxByte<8; idxByte++)
    {
    pktSetup[idxByte] = ptrData[idxByte];
    uartWriteHex(ptrData[idxByte]);
    }

if( ptrData[0] == 0x00 && ptrData[1] == 5 )
    { // Set Address
    uartWriteStr(" Addr 0x");
    uartWriteHex(ptrData[2]);
    newAddr = 0x80 | ptrData[2];
    // Write ACK
    epSetFlags(EP0OUT, 0, EP_STALL);
    epSetOffset(EP0IN, 0);
    epSetNBytes(EP0IN, 0, 0);
    epSetFlags(EP0IN, 0, EP_ACTIVE);
    }
else if( ptrData[0] == 0x80 && ptrData[1] == 6 && ptrData[3] == 1 )
    { // Get Descriptor: Device
    uartWriteStr(" Device");
    cntBytes = ptrData[6];
    epSetOffset(EP0IN, 0);
    ptrResp = epGetData(EP0IN, 0);
    for(idxByte=0; idxByte<18; idxByte++)
        ptrResp[idxByte] = pktDeviceDesc[idxByte];
    for(idxByte=18; idxByte<64; idxByte++)
        ptrResp[idxByte] = 0;
    // Note: Linux sometimes asks for just the first few bytes and then
    // repeats the request for the entire packet.
    epSetNBytes(EP0IN, 0, cntBytes);
    epSetFlags(EP0IN, 0, EP_ACTIVE);
#if 0
    REG_USB_DEVCMDSTAT |= DCS_INTNAKCO;
    REG_USB_DEVCMDSTAT &= ~DCS_INTNAKCI;
#endif
    }
else if( ptrData[0] == 0x80 && ptrData[1] == 6 && ptrData[3] == 2 )
    { // Get Descriptor: Configuration
    uartWriteStr(" Config");
    cntBytes = ptrData[6];
    epSetOffset(EP0IN, 0);
    ptrResp = epGetData(EP0IN, 0);
    for(idxByte=0; idxByte<32; idxByte++)
        ptrResp[idxByte] = pktConfigDesc[idxByte];
    for(; idxByte<64; idxByte++)
        ptrResp[idxByte] = 0;
    epSetNBytes(EP0IN, 0, cntBytes);
    epSetFlags(EP0IN, 0, EP_ACTIVE);
#if 0
    REG_USB_DEVCMDSTAT |= DCS_INTNAKCO;
    REG_USB_DEVCMDSTAT &= ~DCS_INTNAKCI;
#endif
    }
else if( ptrData[0] == 0x80 && ptrData[1] == 6 && ptrData[3] == 3 )
    { // Get Descriptor: String
    uartWriteStr(" String 0x");
    uartWriteHex(ptrData[2]);
    // Language code disregarded
    uartWriteStr(" Lang 0x");
    uartWriteHex(ptrData[4]);
    uartWriteHex(ptrData[5]);
    cntBytes = ptrData[6];
    epSetOffset(EP0IN, 0);
    ptrResp = epGetData(EP0IN, 0);

    if( ptrData[2] == 0 )
        {
        for(idxByte=0; idxByte<6; idxByte++)
            ptrResp[idxByte] = pktStringDesc[idxByte];
        }
    else
        {
        const char *ptrStr;
        switch( ptrData[2] )
            {
            case 1:
                ptrStr = strManu;
                break;
            case 2:
                ptrStr = strProd;
                break;
            default:
                ptrStr = strNone;
                break;
            }
        idxByte = 2;
        while( *ptrStr )
            {
            ptrResp[ idxByte++ ] = *ptrStr;
            ptrResp[ idxByte++ ] = 0;
            ptrStr++;
            }
        ptrResp[0] = idxByte;
        ptrResp[1] = 0x03;
        }

    // Cap packet length if longer than what host specified
    if( idxByte > cntBytes )
        idxByte = cntBytes;
    epSetNBytes(EP0IN, 0, idxByte);
    epSetFlags(EP0IN, 0, EP_ACTIVE);
    }
else if( ptrData[0] == 0x00 && ptrData[1] == 9 )
    { // Set config
    uartWriteStr(" SetCfg 0x");
    uartWriteHex( ptrData[2] );
    uartWriteHex( ptrData[3] );
    // Deactivate data endpoints
    epDeactivate(EP1OUT);
    epDeactivate(EP1IN);
    // Activate EP1 Out
    epSetOffset(EP1OUT, 0);
    epSetNBytes(EP1OUT, 0, 64);
    epClrFlags(EP1OUT, 0, EP_STALL);
    epClrFlags(EP1OUT, 0, EP_DISABLE);
    epSetFlags(EP1OUT, 0, EP_ACTIVE);
    // Activate EP1 In, responds with no data.
    epSetOffset(EP1IN, 0);
    epSetNBytes(EP1IN, 0, 0);
    epSetFlags(EP1IN, 0, EP_STALL);
    epClrFlags(EP1IN, 0, EP_DISABLE);
    epSetFlags(EP1IN, 0, EP_ACTIVE);
    // Ack request
    epSetFlags(EP0OUT, 0, EP_STALL);
    epSetOffset(EP0IN, 0);
    epSetNBytes(EP0IN, 0, 0);
    epSetFlags(EP0IN, 0, EP_ACTIVE);
    }
else
    {
    uartWriteStr(" NotImpl! ");
    uartWriteHex( ptrData[1] );
    uartWriteStr(" 0x");
    uartWriteHex( ptrData[2] );
    uartWriteHex( ptrData[3] );
    epSetFlags(EP0IN, 0, EP_STALL);
    epSetFlags(EP0OUT, 0, EP_STALL);
    }
}


void handleOut0(void)
{
if(pktSetup[0] & 0x80)
    {
    // Device to Host
    // Status Out Stage (receive Ack)
    uartWriteStr(" D2H ");
    epSetFlags(EP0OUT, 0, EP_STALL);
    REG_USB_INTSTAT = INT_EP0IN;
    }
else
    {
    // Host to Device
    // Assumes any data has already been all sent
    uartWriteStr(" H2D ");
    epSetNBytes(EP0IN, 0, 0);
    epSetFlags(EP0IN, 0, EP_STALL);
    epSetFlags(EP0IN, 0, EP_ACTIVE);
    epDeactivate(EP0OUT);
    epSetFlags(EP0OUT, 0, EP_STALL);
    }
}


void handleIn0(void)
{
if( pktSetup[0] & 0x80 )
    {
    // Device to Host
    // Data In Stage
    uartWriteStr("D2H ");

    // In this firmware it is assumed that all setup data emitted by the
    // device will fit in a single 64-byte packet and that it has all been
    // transmitted when this function has been reached.
    epSetOffset(EP0OUT, 0);
    epSetNBytes(EP0OUT, 0, 0);
    epSetFlags(EP0OUT, 0, EP_STALL);
    epSetFlags(EP0OUT, 0, EP_ACTIVE);
    epSetFlags(EP0IN, 0, EP_STALL);
    REG_USB_INTSTAT = INT_EP0OUT;
    }
else
    {
    // Host to Device
    uartWriteStr("H2D ");
    if( newAddr & 0x80 )
        {
        // newAddr already includes DCS_DEVEN since the upper bit
        // is used as a presence flag. For some reason this upper
        // bit must be written at same time as the address.
        REG_USB_DEVCMDSTAT &= ~DCS_DEVADDR;
        REG_USB_DEVCMDSTAT |= newAddr;
        uartWriteStr("NewAddr");
        newAddr = 0;
        }
    else
        {
        // None of the implemented request types have an outbound
        // data stage so handleSetup() will have already sent ACK
        epSetFlags(EP0IN, 0, EP_STALL);
        }
    }
}


void handleOut1(void)
{
int lenRecv;
int idxRecv;
uint8_t *ptrDataOut;
uint8_t *ptrDataIn;

uartWriteNL();
uartWriteStr("OUT1 ");
epDeactivate(EP1OUT);
lenRecv = 64 - epGetNBytes(EP1OUT, 0);
uartWriteHex( lenRecv );
uartWriteStr(" ");
epSetNBytes(EP1OUT, 0, 64);
epSetOffset(EP1OUT, 0);
ptrDataOut = epGetData(EP1OUT, 0);
for(idxRecv=0; idxRecv < lenRecv; idxRecv++)
    uartWriteChar( ptrDataOut[idxRecv] );

// Echo packet back to host
epDeactivate(EP1IN);
epSetOffset(EP1IN, 0);
ptrDataIn = epGetData(EP1IN, 0);
for(idxRecv=0; idxRecv < lenRecv; idxRecv++)
    ptrDataIn[idxRecv] = ptrDataOut[idxRecv];
epSetNBytes(EP1IN, 0, lenRecv);
epClrFlags(EP1IN, 0, EP_STALL);
epClrFlags(EP1IN, 0, EP_DISABLE);
epSetFlags(EP1IN, 0, EP_ACTIVE);
epClrFlags(EP1OUT, 0, EP_STALL);
epClrFlags(EP1OUT, 0, EP_DISABLE);
epSetFlags(EP1OUT, 0, EP_ACTIVE);
}


void handleIn1(void)
{
// EP1IN finished with. Reactive for 'no data'
uartWriteNL();
uartWriteStr(" IN1");
epDeactivate(EP1IN);
epSetOffset(EP1IN, 0);
epSetNBytes(EP1IN, 0, 0);
epSetFlags(EP1IN, 0, EP_STALL);
epClrFlags(EP1IN, 0, EP_DISABLE);
epSetFlags(EP1IN, 0, EP_ACTIVE);
REG_USB_INTSTAT = INT_EP0IN;
}


void handleUSB(void)
{
uint32_t val;
uint32_t bits;

if( REG_USB_INTSTAT & INT_DEVSTAT )
    {
    // Check Device Status Register
    val = REG_USB_DEVCMDSTAT;
    if( 1 )
        {
        uartWriteNL();
        uartWriteStr("DCS ");
        uartWriteHex(val>>24);
        uartWriteHex(val>>16);
        uartWriteHex(val>>8);
        uartWriteHex(val);
        printInfo();
        }
    if( val & (1<<26) )
        {
        uartWriteStr("Reset");
        REG_USB_DEVCMDSTAT |= (1<<26);
        resetUSB();
        }
    REG_USB_INTSTAT = INT_DEVSTAT;
    return;
    }

// 1ms timer. For periodic xfer
if( REG_USB_INTSTAT & INT_FRAME )
    REG_USB_INTSTAT = INT_FRAME;

while( REG_USB_INTSTAT & (INT_EP0OUT|INT_EP0IN) )
    {
    if( REG_USB_INTSTAT & INT_EP0OUT )
        {
        REG_USB_INTSTAT = INT_EP0OUT;
        uartWriteNL();
        uartWriteStr("OUT0");
        if( REG_USB_DEVCMDSTAT & DCS_SETUP )
            {
            uartWriteStr(" Setup ");
            epDeactivate(EP0OUT);
            epDeactivate(EP0IN);
            epClrFlags(EP0OUT, 0, EP_ACTIVE);
            epClrFlags(EP0OUT, 0, EP_STALL);
            epClrFlags(EP0IN, 0, EP_ACTIVE);
            epClrFlags(EP0IN, 0, EP_STALL);
            REG_USB_INTSTAT = INT_EP0IN;
            REG_USB_DEVCMDSTAT &= ~(DCS_INTNAKCO|DCS_INTNAKCI);
            handleSetup();
            REG_USB_DEVCMDSTAT |= DCS_SETUP;
            }
        else
            {
            handleOut0();
            }
        }
    if( REG_USB_INTSTAT & INT_EP0IN )
        {
        // EP0IN
        REG_USB_INTSTAT = INT_EP0IN;
        uartWriteNL();
        uartWriteStr(" IN0 ");
        handleIn0();
        }
    }

if( REG_USB_INTSTAT & INT_EP1OUT )
    handleOut1();

if( REG_USB_INTSTAT & INT_EP1IN )
    handleIn1();

bits = REG_USB_INTSTAT;
if(bits & ~0x40000000)
    {
    // Unserviced interrupt.
    uartWriteNL();
    uartWriteStr("INTR 0x");
    uartWriteHex(bits>>24);
    uartWriteHex(bits>>16);
    uartWriteHex(bits>>8);
    uartWriteHex(bits);
    }
}



/****************************************************************************
 ** Entrypoints
 ****************************************************************************/

void ARMmain(void)
{
REG(0x40048080) |= 1 << 16;   // Enable IOCON clock
setupClock();
setupRS232();
setupUSB();

while(1);
}

void ARMirq(uint32_t idIRQ)
{
switch(idIRQ)
    {
    case 22:
    case 23:
        handleUSB();
        break;
    default:
        uartWriteStr("IRQ ");
        uartWriteHex(idIRQ);
        uartWriteNL();
    }
}

