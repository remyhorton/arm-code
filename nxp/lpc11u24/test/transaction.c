/*
    transaction.c - USB transaction using libUSB
    (C) Remy Horton, 2023
    SPDX-License-Identifier: BSD-3-Clause

    Finds the first EndPoints in the first interface for whatever
    the active confugration is and does a write and two reads.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libusb.h>


/* If code is nonzero, print corresponding error and exit program */
int check(const char *function, const int code)
    {
    if( code == 0 )
        return code;
    fprintf(
        stderr,
        "ERROR: %s() returned %s\n",
        function,
        libusb_error_name(code)
        );
    exit(1);
    }


void printString(libusb_device_handle *hdl, const int idx)
    {
    unsigned char buffer[128];
    int retcode;
    if( idx == 0 )
        {
        printf("<none>\n");
        return;
        }
    retcode = libusb_get_string_descriptor_ascii(hdl, idx, buffer, 128);
    if( retcode <= 0 )
        printf("<error>\n");
    else
        printf("%s\n", buffer);
    }


int main(int argc, char **argv)
    {
    libusb_device **listDevices;
    libusb_device_handle *handle;
    struct libusb_device_descriptor info;
    struct libusb_config_descriptor *config;
    const struct libusb_interface_descriptor *interface;
    uint8_t buffer[65];
    int addrEP0;
    int addrEP1;
    int idxDevice;
    int cntDevices;
    int cntReads;
    int lenTransmission;

    if(argc != 2)
        {
        fprintf(stderr, "USAGE: %s <message>\n", argv[0]);
        exit(1);
        }

    check("libusb_init", libusb_init(NULL));

    cntDevices = libusb_get_device_list(NULL, &listDevices);
    for(idxDevice=0; idxDevice<cntDevices; idxDevice++)
        {
        libusb_get_device_descriptor(listDevices[idxDevice], &info);
        if( info.idVendor == 0x1209 && info.idProduct == 0x0001 )
            {
            libusb_device *device = listDevices[idxDevice];
            printf("USB demo board found as device %i\n", idxDevice);
            check("libusb_open", libusb_open(device, &handle));

            // Some device info
            printf("        Manufacturer: ");
            printString(handle, info.iManufacturer);
            printf("             Product: ");
            printString(handle, info.iProduct);
            printf("       Serial number: ");
            printString(handle, info.iSerialNumber);

            // Find endpoint addresses
            check(
                "libusb_get_config_descriptor",
                libusb_get_config_descriptor(device, 0, &config)
                );
            interface = &config->interface[0].altsetting[0];
            addrEP0 = interface->endpoint[0].bEndpointAddress;
            if( addrEP0 & 0x80 )
                {
                addrEP1 = addrEP0;
                addrEP0 = interface->endpoint[1].bEndpointAddress;
                }
            else
                {
                addrEP1 = interface->endpoint[1].bEndpointAddress;
                }
            printf("    Interface number: %i\n", interface->bInterfaceNumber);
            printf("   EndPoint OUT Addr: %i\n", addrEP0);
            printf("   EndPoint IN  Addr: %i\n", addrEP1);

            // Get handle for I/O
            check(
                "libusb_claim_interface",
                libusb_claim_interface(handle, interface->bInterfaceNumber)
                );

            // Send some data
            check(
                "libusb_interrupt_transfer",
                libusb_interrupt_transfer(
                    handle,
                    addrEP0,
                    (uint8_t*)argv[1],
                    strlen(argv[1]),
                    &lenTransmission,
                    0)
                );
            printf("Tx %i bytes\n",lenTransmission);

            // Read some data
            for(cntReads=0; cntReads < 2; cntReads++)
                {
                check(
                    "libusb_interrupt_transfer",
                    libusb_interrupt_transfer(
                        handle,
                        addrEP1,
                        buffer,
                        64,
                        &lenTransmission,
                        0)
                    );
                printf("Rx %i bytes",lenTransmission);
                if( lenTransmission > 0 )
                    {
                    buffer[lenTransmission] = 0;
                    printf(": %s", buffer);
                    }
                printf("\n");
                }

            // Clean up
            libusb_free_config_descriptor(config);
            libusb_close(handle);
            break; 
            }
    }
    libusb_free_device_list(listDevices, 0);
    libusb_exit(NULL);
    return 0;
    }
