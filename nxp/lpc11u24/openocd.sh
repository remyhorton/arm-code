#!/bin/bash

#export BASE=/opt/openocd/v0.10.0
export BASE=/usr
export SCRIPTS=${BASE}/share/openocd/scripts

${BASE}/bin/openocd \
    -f ${SCRIPTS}/interface/ftdi/olimex-arm-usb-tiny-h.cfg \
    -f ${SCRIPTS}/interface/ftdi/olimex-arm-jtag-swd.cfg \
    -f ${SCRIPTS}/target/lpc11xx.cfg
