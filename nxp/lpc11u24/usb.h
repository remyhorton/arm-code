

/*****************************************************************************
 ** Constants & bitmasks
 *****************************************************************************/

#define REG_USB_DEVCMDSTAT   REG(0x40080000)
#define REG_USB_INFO         REG(0x40080004)
#define REG_USB_EPLISTSTART  REG(0x40080008)
#define REG_USB_DATABUFSTART REG(0x4008000c)
#define REG_USB_LPM          REG(0x40080010)
#define REG_USB_EPSKIP       REG(0x40080014)
#define REG_USB_EPINUSE      REG(0x40080018)
#define REG_USB_EPBUFCFG     REG(0x4008001c)
#define REG_USB_INTSTAT      REG(0x40080020)
#define REG_USB_INTEN        REG(0x40080024)
#define REG_USB_INTSETSTAT   REG(0x40080028)
#define REG_USB_INTROUTING   REG(0x4008002c)
#define REG_USB_EPTOGGLE     REG(0x40080034)

#define REG_SYSAHBCLKCTRL    REG(0x40048080)
#define REG_PDRUNCFG         REG(0x40048238)

#define REG_USB_CLKSEL       REG(0x400480c0)
#define REG_USB_CLKUEN       REG(0x400480c4)
#define REG_USB_CLKDIV       REG(0x400480c8)

#define REG_SYSPLLCTRL       REG(0x40048008)
#define REG_SYSPLLSTAT       REG(0x4004800c)

#define REG_USB_PLLCTRL      REG(0x40048010)
#define REG_USB_PLLSTAT      REG(0x40048014)

#define REG_MAINCLKSEL       REG(0x40048070)

#define REG_SYSPLLCLKSEL     REG(0x40048040)
#define REG_SYSPLLCLKUEN     REG(0x40048044)
#define REG_USB_PLLCLKSEL    REG(0x40048048)
#define REG_USB_PLLCLKUEN    REG(0x4004804c)


#define DCS_DEVADDR         0x0000007f
#define DCS_DEVEN           (1 << 7)
#define DCS_SETUP           (1 << 8)
#define DCS_PLLON           (1 << 9)
#define DCS_RESERVED        (1 << 10)
#define DCS_LPMSUP          (1 << 11)
#define DCS_INTNAKAO        (1 << 12)
#define DCS_INTNAKAI        (1 << 13)
#define DCS_INTNAKCO        (1 << 14)
#define DCS_INTNAKCI        (1 << 15)

#define DCS_DCON            (1 << 16)

#define EP_MASK             0xfc
#define EP_ACTIVE           (1 << 31)
#define EP_DISABLE          (1 << 30)
#define EP_STALL            (1 << 29)
#define EP_RESET            (1 << 28)
#define EP_RATE             (1 << 27)
#define EP_TYPE             (1 << 16)

#define INT_EP0OUT          (1 << 0)
#define INT_EP0IN           (1 << 1)
#define INT_EP1OUT          (1 << 2)
#define INT_EP1IN           (1 << 3)
#define INT_EP2OUT          (1 << 4)
#define INT_EP2IN           (1 << 5)
#define INT_EP3OUT          (1 << 6)
#define INT_EP3IN           (1 << 7)
#define INT_EP4OUT          (1 << 8)
#define INT_EP4IN           (1 << 9)
#define INT_FRAME           (1 << 30)
#define INT_DEVSTAT         (1 << 31)

#define EP0OUT              0
#define EP0IN               1
#define EP1OUT              2
#define EP1IN               3
#define EP2OUT              4
#define EP2IN               5
#define EP3OUT              6
#define EP3IN               7
#define EP4OUT              8
#define EP4IN               9

// CT32B0 timer
#define REG_CT32B0_INT       REG(0x40014000)
#define REG_CT32B0_TCR       REG(0x40014004)
#define REG_CT32B0_TC        REG(0x40014008)
#define REG_CT32B0_PR        REG(0x4001400c)
#define REG_CT32B0_PC        REG(0x40014010)
#define REG_CT32B0_MCR       REG(0x40014014)
#define REG_CT32B0_MR0       REG(0x40014018)
#define REG_CT32B0_MR1       REG(0x4001401c)
#define REG_CT32B0_MR2       REG(0x40014020)
#define REG_CT32B0_MR3       REG(0x40014024)
#define REG_CT32B0_CCR       REG(0x40014028)
#define REG_CT32B0_CR0       REG(0x4001402c)
#define REG_CT32B0_Reserved1 REG(0x40014030)
#define REG_CT32B0_CR1       REG(0x40014034)
#define REG_CT32B0_Reserved2 REG(0x40014038)
#define REG_CT32B0_EMR       REG(0x4001403c)
#define REG_CT32B0_CTCR      REG(0x40014070)
#define REG_CT32B0_PWMC      REG(0x40014074)


#define REG_SYSOSCCTRL       REG(0x40048020)

/*****************************************************************************
 ** Prototypes
 *****************************************************************************/

void uartWriteStr(char *);
void uartWriteHex(const uint32_t);
void uartWriteNL(void);
void setLED1(const int);
void setLED2(const int);


/*****************************************************************************
 ** End-Point table access
 *****************************************************************************/

static inline uint32_t *epGetLine(int idEP, int idBuffer)
    {
    uint32_t offsetToEp = (idEP<<1) | idBuffer;
    return (uint32_t*)(0x20004000 + ( offsetToEp << 2 ));
    }

static inline void epSetOffset(int idEP, int idBuffer)
    {
    uint32_t offsetToEp = (idEP<<1) | idBuffer;
    uint32_t *ptrEP = epGetLine(idEP,idBuffer);
    *ptrEP &= 0xffff0000;
    *ptrEP |= 0x104 + offsetToEp;
    }

static inline void epSetNBytes(int idEP, int idBuffer, int cntBytes)
    {
    uint32_t *ptrEP = epGetLine(idEP,idBuffer);
    *ptrEP &= 0xfc00ffff;
    *ptrEP |= cntBytes << 16;
    }

static inline uint32_t epGetNBytes(int idEP, int idBuffer)
    {
    uint32_t *ptrEP = epGetLine(idEP,idBuffer);
    return (*ptrEP & 0x03ff0000) >> 16;
    }

static inline uint8_t *epGetData(int idEP, int idBuffer)
    {
    uint32_t offsetToEp = (idEP<<1) | idBuffer;
    return (uint8_t*)(0x20004100 + (offsetToEp << 6));
    }

static inline uint32_t epGetFlags(int idEP, int idBuffer)
    {
    uint32_t *ptrEP = epGetLine(idEP,idBuffer);
    return *ptrEP & 0xfc000000;
    }

static inline uint32_t epGetFlag(int idEP, int idBuffer, uint32_t mask)
    {
    uint32_t *ptrEP = epGetLine(idEP,idBuffer);
    return *ptrEP & mask;
    }

static inline void epSetFlags(int idEP, int idBuffer, uint32_t flags)
    {
    uint32_t *ptrEP = epGetLine(idEP,idBuffer);
    *ptrEP |= flags & 0xfc000000;
    }

static inline void epDeactivate(int idEP)
    {
    //FIXME: Unclear whether this should be 1<<(idEP<<1)
    uint32_t bits = 1 << idEP;
    REG_USB_EPSKIP |= bits;
    while(REG_USB_EPSKIP & bits);
    REG_USB_INTSTAT = bits;
    }

static inline void epClrFlags(int idEP, int idBuffer, uint32_t flags)
    {
    uint32_t *ptrEP = epGetLine(idEP,idBuffer);
    if(flags & EP_ACTIVE)
        {
        //uartWriteStr("*");
        //epDeactivate(idEP);
        }
    *ptrEP &= ~(flags&0xfc000000);
    }

