/*
   ke02z.c - Bootstrap for NXP MKE02ZxxV & MKE04Z8Vxx4 Cortex-M0+
   (C) Remy Horton, 2019
   SPDX-License-Identifier: BSD-3-Clause
*/
#include <inttypes.h>
#include <ke02z.h>

extern unsigned __nxpBss;
extern unsigned __nxpBssEnd;
extern unsigned __nxpDataSect;
extern unsigned __nxpDataDst;
extern unsigned __nxpDataEnd;
extern unsigned __nxpStackTop;

__attribute__((weak)) void ARMirq(uint32_t value) { }
__attribute__((weak)) void ARMtimeout(void) { }
__attribute__((weak)) void ARMtrap(void) { }


/* All the callbacks are put into section .callbacks which on linking is
   always put before .text (i.e. all other program code), so the checksum
   value NXP_VERTCRC is not affected by code outside of this file */
#define NXP_VECTCRC 0xefffe780

/****************************************************************************
 ** Wrapped interrupt vectors
 ****************************************************************************/


__attribute__((section(".callbacks")))
static void interruptReset(void)
{
// Copy data segment to RAM
unsigned *src = &__nxpDataSect;
unsigned *dst = &__nxpDataDst;
while( dst < &__nxpDataEnd )
    *dst++ = *src++;

// Zero out BSS
dst = &__nxpBss;
while( dst < &__nxpBssEnd )
    *dst++ = 0;

// Call program entrypoint
ARMmain();
}

__attribute__((section(".callbacks")))
static void interruptIRQ(void)
{
unsigned int valIRQ = *((volatile unsigned int *)0xe000ed04);
ARMirq( (valIRQ & 0x3f) - 16);
}

__attribute__((section(".callbacks")))
static void interruptSysTick(void)
{
ARMtimeout();
}

__attribute__((section(".callbacks")))
static void interruptSVCall(void)
{
// Supervisor call (SVC instruction) - an OS Trap basically
ARMtrap();
}


/****************************************************************************
 ** Unused interrupt vectors
 ****************************************************************************/

__attribute__((section(".callbacks")))
static void interruptNotImpl(void)
{
while(1);
}

__attribute__((section(".callbacks")))
static void interruptPendSV(void)
{
interruptNotImpl();
}

__attribute__((section(".callbacks")))
static void interruptNMI(void)
{
interruptNotImpl();
}

__attribute__((section(".callbacks")))
static void interruptHardFault(void)
{
interruptNotImpl();
}

__attribute__((section(".callbacks")))
static void interruptReserved(void)
{
interruptNotImpl();
}

__attribute__((section(".stack"),used)) unsigned *ptrStackTop = &__nxpStackTop;
__attribute__((section(".handlers"),used)) void (*vecHandlers[15+32])() = {
    // Table 3-8, Pages 50-51
    // Standard Cortex-M+ interrupts
	interruptReset,
	interruptNMI,
	interruptHardFault,
	interruptReserved, interruptReserved, interruptReserved,
    interruptReserved,
	///(void*)NXP_VECTCRC, // Checksum
	interruptReserved, interruptReserved, interruptReserved,
	interruptSVCall,
	interruptReserved,
    interruptReserved,
	interruptPendSV,
	interruptSysTick,

	// Device-specific interrupts. All routed via interruptIRQ()
	interruptIRQ,	interruptIRQ,	interruptIRQ,	interruptIRQ,
	interruptIRQ,	interruptIRQ,	interruptIRQ,	interruptIRQ,
	interruptIRQ,	interruptIRQ,	interruptIRQ,	interruptIRQ,
	interruptIRQ,	interruptIRQ,	interruptIRQ,	interruptIRQ,
	interruptIRQ,	interruptIRQ,	interruptIRQ,	interruptIRQ,
	interruptIRQ,	interruptIRQ,	interruptIRQ,	interruptIRQ,
	interruptIRQ,	interruptIRQ,	interruptIRQ,	interruptIRQ,
	interruptIRQ,	interruptIRQ,	interruptIRQ,	interruptIRQ,
};

// Flash configuration area. See Table 18-6 on Page 225
__attribute__((section(".config"),used)) unsigned char config[16] = {
    0,0,0,0,0,0,0,0,    // Backdoor comparison key
    0,0,0,0,            // Reserved
    0xff,               // EEPROM protection
    0xff,               // Flash protection
    0x82,               // Flash security
    0                   // Flash nonvolatile
};
