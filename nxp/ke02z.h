/*
   ke02z.h - Bootstrap for NXP MKE02ZxxV Cortex-M0+ (header)
   (C) Remy Horton, 2019
   SPDX-License-Identifier: BSD-3-Clause
*/

#define REG8(r)  (*((volatile uint8_t  *)r))
#define REG16(r) (*((volatile uint16_t *)r))
#define REG32(r) (*((volatile uint32_t *)r))
#define REG(r)   (*((volatile uint32_t *)r))

// These are ARM standard
#define REG_ISER 0xe000e100  // Interrupt set enable
#define REG_ICER 0xe000e180  // Interrupt clear enable
#define REG_ISPR 0xe000e200  // Interrupt set pending
#define REG_ICPR 0xe000e280  // Interrupt clear pending


// Clock sources
#define REG_ICS_CTL1 REG(0x40064000)


// SIM (System Integration Module_ regisers
#define REG_SYS_SRSID       REG32(0x40048000)
#define REG_SYS_OPTIONS     REG32(0x40048004)
#define REG_SYS_PINSEL      REG32(0x40048008)
#define REG_SYS_CLKGATE     REG32(0x4004800c)
#define REG_SYS_UUIDL       REG32(0x40048010)
#define REG_SYS_UUIDH       REG32(0x40048014)
#define REG_SYS_BUSDIV      REG32(0x40048018)

// Watchdog registers
#define REG_WDOG_CS1        REG8(0x40052000)
#define REG_WDOG_CS2        REG8(0x40052001)
#define REG_WDOG_COUNT     REG16(0x40052002) // For convenience
#define REG_WDOG_CNTH       REG8(0x40052002)
#define REG_WDOG_CNTL       REG8(0x40052003)
#define REG_WDOG_TIMEOUT   REG16(0x40052004)
#define REG_WDOG_TIMEOUTH   REG8(0x40052004)
#define REG_WDOG_TIMEOUTL   REG8(0x40052005)
#define REG_WDOG_WNDREGH    REG8(0x40052006)
#define REG_WDOG_WNDREGL    REG8(0x40052007)

// 'Fast' GPIO registers
// Aliases of the GPIO registers at 0x400ff0xx.
// No idea if there is any actual difference.
#define REG_FGPIOA_DATAOUT  REG(0xf8000000)
#define REG_FGPIOA_SETOUT   REG(0xf8000004)
#define REG_FGPIOA_CLROUT   REG(0xf8000008)
#define REG_FGPIOA_TOGGLE   REG(0xf800000C)
#define REG_FGPIOA_DATAIN   REG(0xf8000010)
#define REG_FGPIOA_DATADIR  REG(0xf8000014)
#define REG_FGPIOA_DISABLE  REG(0xf8000018) // High-Z

#define REG_FGPIOB_DATAOUT  REG32(0xf8000040)
#define REG_FGPIOB_SETOUT   REG32(0xf8000044)
#define REG_FGPIOB_CLROUT   REG32(0xf8000048)
#define REG_FGPIOB_TOGGLE   REG32(0xf800004C)
#define REG_FGPIOB_DATAIN   REG32(0xf8000050)
#define REG_FGPIOB_DATADIR  REG32(0xf8000054)
#define REG_FGPIOB_DISABLE  REG32(0xf8000058)

// FTM registers (base addr: 0x4003x??? x=8,9,a)
#define REG_FTM0_SC         REG(0x40038000)
#define REG_FTM0_CNT        REG(0x40038004)
#define REG_FTM0_MOD        REG(0x40038008)
#define REG_FTM0_C0SC       REG(0x4003800c)
#define REG_FTM0_C0V        REG(0x40038010)
#define REG_FTM0_C1SC       REG(0x40038014)
#define REG_FTM0_C1V        REG(0x40038018)
#define REG_FTM0_C2SC       REG(0x4003801c)
#define REG_FTM0_C2V        REG(0x40038020)
#define REG_FTM0_C3SC       REG(0x40038024)
#define REG_FTM0_C3V        REG(0x40038028)
#define REG_FTM0_C4SC       REG(0x4003802c)
#define REG_FTM0_C4V        REG(0x40038030)
#define REG_FTM0_C5SC       REG(0x40038034)
#define REG_FTM0_C5V        REG(0x40038038)
#define REG_FTM0_C6SC       REG(0x4003803c)
#define REG_FTM0_C6V        REG(0x40038040)
#define REG_FTM0_C7SC       REG(0x40038044)
#define REG_FTM0_C7V        REG(0x40038048)
#define REG_FTM0_CNTIN      REG(0x4003804c)
#define REG_FTM0_STATUS     REG(0x40038050)
#define REG_FTM0_MODE       REG(0x40038054)
#define REG_FTM0_SYNC       REG(0x40038058)
#define REG_FTM0_OUTINIT    REG(0x4003805c)
#define REG_FTM0_OUTMASK    REG(0x40038060)
#define REG_FTM0_COMBINE    REG(0x40038064)
#define REG_FTM0_DEADTIME   REG(0x40038068)
#define REG_FTM0_EXTTRIG    REG(0x4003806c)
#define REG_FTM0_POL        REG(0x40038070)
#define REG_FTM0_FMS        REG(0x40038074)
#define REG_FTM0_FILTER     REG(0x40038078)
#define REG_FTM0_FLTCTRL    REG(0x4003807c)
// Why gap here??
#define REG_FTM0_CONF       REG(0x40038084)
#define REG_FTM0_FLTPOL     REG(0x40038088)
#define REG_FTM0_SYNCONF    REG(0x4003808c)
#define REG_FTM0_INVCTRL    REG(0x40038090)
#define REG_FTM0_SWOCTRL    REG(0x40038094)
#define REG_FTM0_PWMLOAD    REG(0x40038098)


// PIC (Periodic Interrupt Timer).
// KE02 has 1 timer with 2 channel.
#define REG_PIT_MCR         REG(0x40037000)
#define REG_PIT_LDVAL0      REG(0x40037100)
#define REG_PIT_CVAL0       REG(0x40037104)
#define REG_PIT_TCTRL0      REG(0x40037108)
#define REG_PIT_TFLG0       REG(0x4003710c)
#define REG_PIT_LDVAL1      REG(0x40037110)
#define REG_PIT_CVAL1       REG(0x40037114)
#define REG_PIT_TCTRL1      REG(0x40037118)
#define REG_PIT_TFLG1       REG(0x4003711c)

// Real-time clock
#define REG_RTC_SC  REG(0x4003d000)
#define REG_RTC_MOD REG(0x4003d004)
#define REG_RTC_CNT REG(0x4003d008)

// I2C registers.
#define REG_I2C0_ADDR1      REG8(0x40066000)
#define REG_I2C0_FDIV       REG8(0x40066001)
#define REG_I2C0_CTRL1      REG8(0x40066002)
#define REG_I2C0_STAT       REG8(0x40066003)
#define REG_I2C0_DATA       REG8(0x40066004)
#define REG_I2C0_CTRL2      REG8(0x40066005)
#define REG_I2C0_GFILT      REG8(0x40066006)
#define REG_I2C0_RADDR      REG8(0x40066007)
#define REG_I2C0_SMBUS      REG8(0x40066008)
#define REG_I2C0_ADDR2      REG8(0x40066009)
#define REG_I2C0_SLTHL      REG8(0x4006600a)
#define REG_I2C0_SLTLO      REG8(0x4006600b)

// I2C bit patterns
#define I2C_RXAK    0x01
#define I2C_INTF    0x02
#define I2C_SRW     0x04
#define I2C_RAM     0x08
#define I2C_ARBL    0x10
#define I2C_BUSY    0x20
#define I2C_IAAS    0x40
#define I2C_TXC     0x80

#define I2C_WUEN    0x02
#define I2C_RTSA    0x04
#define I2C_TXAK    0x08
#define I2C_TX      0x10
#define I2C_MST     0x20
#define I2C_INTE    0x40
#define I2C_EN      0x80

// RS232 registers (3x UARTs)
#define REG_UART0_BAUDH     REG8(0x4006a000)
#define REG_UART0_BAUDL     REG8(0x4006a001)
#define REG_UART0_CTRL1     REG8(0x4006a002)
#define REG_UART0_CTRL2     REG8(0x4006a003)
#define REG_UART0_STAT1     REG8(0x4006a004)
#define REG_UART0_STAT2     REG8(0x4006a005)
#define REG_UART0_CTRL3     REG8(0x4006a006)
#define REG_UART0_DATA      REG8(0x4006a007)
#define REG_UART1_BAUDH     REG8(0x4006b000)
#define REG_UART1_BAUDL     REG8(0x4006b001)
#define REG_UART1_CTRL1     REG8(0x4006b002)
#define REG_UART1_CTRL2     REG8(0x4006b003)
#define REG_UART1_STAT1     REG8(0x4006b004)
#define REG_UART1_STAT2     REG8(0x4006b005)
#define REG_UART1_CTRL3     REG8(0x4006b006)
#define REG_UART1_DATA      REG8(0x4006b007)
#define REG_UART2_BAUDH     REG8(0x4006c000)
#define REG_UART2_BAUDL     REG8(0x4006c001)
#define REG_UART2_CTRL1     REG8(0x4006c002)
#define REG_UART2_CTRL2     REG8(0x4006c003)
#define REG_UART2_STAT1     REG8(0x4006c004)
#define REG_UART2_STAT2     REG8(0x4006c005)
#define REG_UART2_CTRL3     REG8(0x4006c006)
#define REG_UART2_DATA      REG8(0x4006c007)


void ARMmain(void);
void ARMtrap(void);
void ARMtimeout(void);
void ARMirq(uint32_t);
