#!/usr/bin/env python3
## kinetis-i2c-params.py - Kinetis KE02 I2C BAUD parameter calculation
## (c) Remy Horton, 2019
## SPDX-License-Identifier: BSD-3-Clause

import sys

# Table 30-28 (page 534) KE02 reference manual
dataset = [
    (20,7,6,11),            (22,7,7,12),            (24,8,8,13),
    (26,8,9,14),            (28,9,10,15),           (30,9,11,16),
    (34,10,13,18),          (40,10,16,21),          (28,7,10,15),
    (32,7,12,17),           (36,9,14,19),           (40,9,16,21),
    (44,11,18,23),          (48,11,20,25),          (56,13,24,29),
    (68,13,30,35),          (48,9,18,25),           (56,9,22,29),
    (64,13,26,33),          (72,13,30,37),          (80,17,34,41),
    (88,17,38,45),          (104,21,46,53),         (128,21,58,65),
    (80,9,38,41),           (96,9,46,49),           (112,17,54,57),
    (128,17,62,65),         (144,25,70,73),         (160,25,78,81),
    (192,33,94,97),         (240,33,118,121),       (160,17,78,81),
    (192,17,94,97),         (224,33,110,113),       (256,33,126,129),
    (288,49,142,145),       (320,49,158,161),       (384,65,190,193),
    (480,65,238,241),       (320,33,158,161),       (384,33,190,193),
    (448,65,222,225),       (512,65,254,257),       (576,97,286,289),
    (640,97,318,321),       (768,129,382,385),      (960,129,478,481),
    (640,65,318,321),       (768,65,382,385),       (896,129,446,449),
    (1024,129,510,513),     (1152,193,574,577),     (1280,193,638,641),
    (1536,257,766,769),     (1920,257,958,961),     (1280,129,638,641),
    (1536,129,766,769),     (1792,257,894,897),     (2048,257,1022,1025),
    (2304,385,1150,1153),   (2560,385,1278,1281),   (3072,513,1534,1537),
    (3840,513,1918,1921)
    ]

def calcRow(valBusSpeed, valMult, idxDiv):
    listMult = [1,2,4]
    valMult = listMult[idxMult]
    valDiv = dataset[idxDiv][0]
    baud = valBusSpeed / (valMult * valDiv)
    return (int(baud), idxMult, idxDiv)

if __name__ == "__main__":
    if len(sys.argv) not in [2,3,4]:
        print( "USAGE: {0} <busdiv> [target] [clock]".format(sys.argv[0]) )
        sys.exit(1)
    if len(sys.argv) in [3,4]:
        target = int(sys.argv[2])
    else:
        target = 0
    if len(sys.argv) == 4:
        valBusSpeed = int(sys.argv[3]) / int(sys.argv[1])
    else:
        valBusSpeed = 31827200 / int(sys.argv[1])

    listParams = []
    for idxMult in [0,1,2]:
        for idxDiv in range(0,len(dataset)):
            listParams.append( calcRow(valBusSpeed, idxMult, idxDiv) )
    listParams.sort( key=lambda x : abs(x[0]-target) )
    if target > 0:
        listParams = listParams[:10]

    print("BAUD      MULT ICR")
    print("--------- ---- ---")
    for (baud,mult,div) in listParams:
        print("{0:<8}  {1}    {2:x}".format(baud,mult,div))

