/*
    i2c.c - Demonstration of master I2C on ke02z64
    (C) Remy Horton, 2019
    SPDX-License-Identifier: BSD-3-Clause

    Tested using a Texas Instruments PCF8574 I/O expander
*/
#include <inttypes.h>
#include <ke02z.h>


void pitSetup(void)
{
// Activate timer clocking
REG(0x4004800c) |= 0x02;
REG_PIT_MCR = 0;
}

void pitDelay(const uint32_t millis)
{
REG_PIT_LDVAL0 = millis*8000lu;
REG_PIT_TFLG0 = 1;
REG_PIT_TCTRL0 = 0x01;
while(REG_PIT_TFLG0 == 0);
REG_PIT_TCTRL0 = 0;
}


/****************************************************************************
 * I2C Master routines
 ****************************************************************************/

void i2cSetup(void)
{
REG32(0x4004800c) |= 1 << 17;   // Enable I2C clocking
REG8(0x40066001) = 0x1d;        // Data rate
REG8(0x40066002) |= I2C_EN;     // Enable (Ctrl1)
}

static inline uint8_t i2cStatBit(const uint8_t mask)
{
return REG8(0x40066003) & mask;
}

static inline void i2cClearBit(const uint8_t mask)
{
REG8(0x40066003) = mask;
}

void i2cWrite(uint8_t addr, int reg1, int reg2, int len, uint8_t *data)
{
int pos = 0;

REG8(0x40066002) |= I2C_TX;
REG8(0x40066002) |= I2C_MST;
REG8(0x40066004) = addr & 0xfe;

if(reg1 != -1)
    {
    if(reg2 != -1)
        pos = -2;
    else
        pos = -1;
    }
while(1)
    {
    while(! i2cStatBit(I2C_INTF) )
        {
        }
    i2cClearBit(I2C_INTF);
    if( i2cStatBit(I2C_RXAK) )
        {
        break;
        }
    if(pos == -2)
        REG8(0x40066004) = reg1;
    else if(pos == -1)
        {
        if(reg2 != -1)
            REG8(0x40066004) = reg2;
        else
            REG8(0x40066004) = reg1;
        }
    else if(pos == len)
        {
        break;
        }
    else
        {
        REG8(0x40066004) = data[pos];
        //rs232WriteHex(data[pos]); 
        }
    pos++;
    }
REG8(0x40066002) &= ~I2C_MST;
}


void i2cRead(uint8_t addr, int reg1, int reg2, int len, uint8_t *data)
{
int pos = -1;
uint8_t bite;

REG8(0x40066002) |= I2C_TX;
REG8(0x40066002) |= I2C_MST;
REG8(0x40066004) = addr | 0x01;
REG8(0x40066002) &= ~I2C_TXAK;

if(reg1 != -1)
    {
    if(reg2 != -1)
        pos = -3;
    else
        pos = -2;
    }
while(1)
    {
    while(! i2cStatBit(I2C_INTF) )
        {
        }
    i2cClearBit(I2C_INTF);
    if(pos == -3)
        REG8(0x40066004) = reg1;
    else if(pos == -2)
        {
        if(reg2 != -1)
            REG8(0x40066004) = reg2;
        else
            REG8(0x40066004) = reg1;
        }
    else if(pos == -1)
        {
        REG8(0x40066002) &= ~I2C_TX;
        bite = REG8(0x40066004);  // Clear out junk 
        }
    else if(pos == len-1)
        {
        REG8(0x40066002) &= ~I2C_MST;
        bite = REG8(0x40066004);
        data[pos] = bite;
        break;
        }
    else
        {
        if(pos == len-2)
            REG8(0x40066002) |= I2C_TXAK;
        bite = REG8(0x40066004);
        data[pos] = bite;
        }
    pos++;
    }
REG8(0x40066002) &= ~I2C_MST;
}


void ARMmain(void)
{
// Disable watchdog.
REG16(0x40052002) = 0x20c5;
REG16(0x40052002) = 0x28d9;
REG8(0x40052001) = 0;
REG16(0x40052004) = 0xffff;
REG8(0x40052000) = 0;

pitSetup();
i2cSetup();

uint8_t data[4] = "\xfe\xff";

// Blink pin 0
while(1)
    {
    pitDelay(1000);
    i2cWrite(0x40, -1, -1, 1, &data[0]);
    pitDelay(1000);
    i2cWrite(0x40, -1, -1, 1, &data[1]);
    }

// Poll pins
while(1)
    {
    pitDelay(1000);
    i2cRead(0x40, -1, -1, 1, &data[0]);
    }
}

