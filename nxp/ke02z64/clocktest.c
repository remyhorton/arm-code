/*
    clocktest.c - Toggle KE02 pins with period of 16,000 clock cycles
    (C) Remy Horton, 2019
    SPDX-License-Identifier: BSD-3-Clause
*/
#include <inttypes.h>
#include <ke02z.h>

void ClockTest(void)
{
//REG8(0x40064001) = 0x00; // 1:1
//REG8(0x40064001) = 0x20; // 1:2    994.6Hz (default)
//REG8(0x40064001) = 0x40; // 1:4    497.3Hz
//REG8(0x40064001) = 0x60; // 1:8    248.6-248.7Hz
//REG8(0x40064001) = 0x80; // 1:16   124.3-124.4Hz
//REG8(0x40064001) = 0xa0; // 1:32
//REG8(0x40064001) = 0xc0; // 1:64
//REG8(0x40064001) = 0xe0; // 1:128

// Activate timer clocking
REG(0x4004800c) |= 0x02;
REG_PIT_MCR = 0;

REG(0xf8000014) = 0x030fff00;
while(1)
    {
    REG(0xf8000004) = 0x030fff00;
    REG_PIT_LDVAL0 = 8000lu;
    REG_PIT_TFLG0 = 1;
    REG_PIT_TCTRL0 = 0x01;
    while( REG_PIT_TFLG0 == 0);
    REG_PIT_TCTRL0 = 0;

    REG(0xf8000008) = 0x030fff00;
    REG_PIT_LDVAL0 = 8000lu;
    REG_PIT_TFLG0 = 1;
    REG_PIT_TCTRL0 = 0x01;
    while( REG_PIT_TFLG0 == 0);
    REG_PIT_TCTRL0 = 0;
    }
}

void ARMmain(void)
{
// Disable watchdog.
REG16(0x40052002) = 0x20c5;
REG16(0x40052002) = 0x28d9;
REG8(0x40052001) = 0;
REG16(0x40052004) = 0xffff;
REG8(0x40052000) = 0;

ClockTest();
}

