#!/usr/bin/env python3
## kinetis-rs232-params.py - Kinetis KE02 RS232 parameter calculation
## (c) Remy Horton, 2019
## SPDX-License-Identifier: BSD-3-Clause

import sys

def calcValues(clock, BDiv, valTarget):
    valBusSpeed = clock // BDiv
    listRawParams = []
    for valBR in range(1,200):
        baud = int(valBusSpeed / (16*valBR))
        error = baud - valTarget
        perc = ((error / valTarget) * 100.0)
        listRawParams.append( (baud,valBR,error,perc) )
    return listRawParams

if __name__ == "__main__":
    if len(sys.argv) not in [3,4]:
        print("USAGE: {0} <busdiv> <targetBAUD> [clock]".format(
            sys.argv[0])
            )
        sys.exit(1)
    # 31.82MHz found experimentally
    clock = 31827200
    bdiv = int(sys.argv[1])
    baud = int(sys.argv[2])
    if len(sys.argv) == 4:
        clock = int(sys.argv[3])
    listParams = sorted(
        calcValues(clock,bdiv,baud),
        key=lambda x : abs(x[2])
        )
    print("  BAUD rate  SBR   error (%)")
    print("  ---------  ----  --------------")
    for baud,BR,err,perc in listParams[:5]:
        print("  {0:<9}  {1:<4}  {2:+} ({3:.2f}%)".format(
            baud,BR,err,abs(perc)))

