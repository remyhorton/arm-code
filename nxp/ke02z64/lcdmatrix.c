/*
    lcdmatrix.c - LCD Matrix driver firmware
    (C) Remy Horton, 2019
    SPDX-License-Identifier: BSD-3-Clause
*/
#include <inttypes.h>
#include <ke02z.h>

            /* Ports: DDCCBBAA */
#define LCD_CS1     0x00020000
#define LCD_CS2     0x00010000
#define LCD_DI      0x00040000
#define LCD_RW      0x00080000
#define LCD_EN      0x01000000
#define LCD_RESET   0x02000000
#define LCD_

uint64_t columns[128];

void lcdPulseWait(void)
    {
    REG(0xf8000004) = LCD_EN;
    REG_PIT_LDVAL0 = 100lu;
    REG_PIT_TFLG0 = 1;
    REG_PIT_TCTRL0 = 0x01;
    while( REG_PIT_TFLG0 == 0);
    REG_PIT_TCTRL0 = 0;
    REG(0xf8000008) = LCD_EN;
    }

void lcdSetup(void)
    {
    /*  S12.2.2: System Options Register
        Disable NMI (PTB4); leave SWD (PTA4) & Reset (PTA5) enabled.
        Don't get this one wrong, otherwise you may end up with an
        unflashable chip. Like i did.. :(
    */
    REG(0x40048004) = 0x0c;

    // Port mask
    REG(0xf8000014) = 0x030fff00;
    REG(0xf8000008) = 0x030fff00;

    // Enable chips
    REG(0xf8000004) = LCD_CS2 | LCD_RESET;
    REG(0xf8000004) = 0x3f << 8;
    lcdPulseWait();
    REG(0xf8000008) = LCD_CS2;
    REG(0xf8000004) = LCD_CS1;
    lcdPulseWait();
    }

void lcdSetChip(uint8_t idChip)
    {
    REG(0xf8000008) = LCD_CS1 | LCD_CS2;
    if( idChip == 0 )
        REG(0xf8000004) = LCD_CS1;
    else
        REG(0xf8000004) = LCD_CS2;
    }

void lcdSetColumn(uint32_t idxCol)
    {
    REG(0xf8000008) = LCD_DI | LCD_RW;
    REG(0xf8000008) = 0xff00;
    REG(0xf8000004) = (0x40 | idxCol) << 8;
    lcdPulseWait();
    }

void lcdSetRow(uint32_t idxRow)
    {
    REG(0xf8000008) = LCD_DI | LCD_RW;
    REG(0xf8000008) = 0xff00;
    REG(0xf8000004) = (0xb8 | idxRow) << 8;
    lcdPulseWait();
    }

void lcdWrite(uint32_t pixels)
    {
    REG(0xf8000008) = LCD_RW;
    REG(0xf8000004) = LCD_DI;
    REG(0xf8000008) = 0xff00;
    REG(0xf8000004) = pixels << 8;
    lcdPulseWait();
    }

void lcdClear()
    {
    int idxCol;
    for(idxCol=0; idxCol < 128; idxCol++)
        columns[idxCol] = 0xaaccccf0f0ff00ff;
    }

void lcdDraw()
    {
    int idxRow;
    int idxCol;
    uint64_t pixels;

    lcdSetChip(0);
    for(idxRow=0; idxRow < 8; idxRow++)
        {
        lcdSetRow(idxRow);
        lcdSetColumn(0);
        for(idxCol=0; idxCol < 64; idxCol++)
            {
            if(idxRow == 0)
                pixels = (columns[idxCol] << 8) & 0xff00;
            else
                pixels = (columns[idxCol] >> (((idxRow-1)<<3)) & 0xff00);
            REG(0xf8000008) = LCD_RW;
            REG(0xf8000004) = LCD_DI;
            REG(0xf8000008) = 0xff00;
            REG(0xf8000004) = pixels;
            lcdPulseWait();
            }
        }

    lcdSetChip(1);
    for(idxRow=0; idxRow < 8; idxRow++)
        {
        lcdSetRow(idxRow);
        lcdSetColumn(0);
        for(idxCol=64; idxCol < 128; idxCol++)
            {
            if(idxRow == 0)
                pixels = (columns[idxCol] << 8) & 0xff00;
            else
                pixels = (columns[idxCol] >> (((idxRow-1)<<3)) & 0xff00);
            REG(0xf8000008) = LCD_RW;
            REG(0xf8000004) = LCD_DI;
            REG(0xf8000008) = 0xff00;
            REG(0xf8000004) = pixels;
            lcdPulseWait();
            }
        }
    }


void ARMmain(void)
{
// Disable watchdog.
REG16(0x40052002) = 0x20c5;
REG16(0x40052002) = 0x28d9;
REG8(0x40052001) = 0;
REG16(0x40052004) = 0xffff;
REG8(0x40052000) = 0;

// Activate timer clocking
REG(0x4004800c) |= 0x02;
REG_PIT_MCR = 0;

lcdSetup();
lcdClear();
lcdDraw();

while(1);
}

void ARMirq(__attribute__((unused)) uint32_t idIRQ)
{
}

