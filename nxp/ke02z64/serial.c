/*
    serial.c - Demonstration of RS232 and slave I2C on ke02z64
    (C) Remy Horton, 2019
    SPDX-License-Identifier: BSD-3-Clause

    RS232 echoes any input back to sender. I2C status output is also
    written to the RS232 connection. For I2C reads the value is the
    number of the byte.
*/
#include <inttypes.h>
#include <ke02z.h>


void pitSetup(void)
{
// Activate timer clocking
REG(0x4004800c) |= 0x02;
REG_PIT_MCR = 0;
}

void pitDelay(void)
{
REG_PIT_LDVAL0 = 8000lu;
REG_PIT_TFLG0 = 1;
REG_PIT_TCTRL0 = 0x01;
while( REG_PIT_TFLG0 == 0);
REG_PIT_TCTRL0 = 0;
}



/****************************************************************************
 * RS232 routines
 ****************************************************************************/

void rs232WriteByte(const uint8_t);


void rs232Setup(void)
    {
    //REG(0x4004800c) |= 1 << 20;   // Enable UART0 clock (not done)
    REG(0x4004800c) |= 1 << 21;     // Enable UART1 clock
    REG8(0x4006b000) = 0;           // No interrupts, 1 stop bit
    REG8(0x4006b001) = 9;           // 9=115200 17=57600 104=9600
    REG8(0x4006b002) = 0;           // 2-wire 8-bit no-parity
    REG8(0x4006b003) = 0x0c;        // Enable Tx & Rx

    // ~1ms delay to allow RS232 to settle down
    pitDelay();

    rs232WriteByte('R');
    rs232WriteByte('e');
    rs232WriteByte('m');
    rs232WriteByte('y');
    rs232WriteByte('\r');
    rs232WriteByte('\n');
    }

void rs232WriteByte(const uint8_t bite)
    {
    while( (REG8(0x4006b004) & 0x80) == 0 );
    REG8(0x4006b007) = bite;
    }

void rs232WriteHex(const uint8_t bite)
    {
    static const uint8_t chars[] = {
        '0','1','2','3','4','5','6','7','8','9',
        'a','b','c','d','e','f'
        };
    rs232WriteByte( chars[ bite >> 4 ] );
    rs232WriteByte( chars[ bite & 0x0f ] );
    }

void procRS232(void)
    {
    uint8_t data;
    if( (REG8(0x4006b004) & 0x20) == 0 )
        return;
    data = REG8(0x4006b007);
    rs232WriteByte(data);
    }


/****************************************************************************
 * I2C Slave routines
 ****************************************************************************/

void i2cProcStart(void);
void i2cProcData(void);

uint8_t i2cData = 0;

void i2cSetup(void)
{
REG32(0x4004800c) |= 1 << 17;   // Enable I2C clocking
REG8(0x40066005) = 0; // 0x10;  // Ctrl Reg 2
REG8(0x40066000) = 0x20;        // Slave addr
//REG8(0x40066001) = 0x1d;      // Data rate
REG8(0x40066002) |= 0x80;       // Enable
}

static inline uint8_t i2cStatBit(const uint8_t mask)
{
return REG8(0x40066003) & mask;
}

static inline void i2cClearBit(const uint8_t mask)
{
REG8(0x40066003) = mask;
}

void i2cProc(void)
{
if(! i2cStatBit(I2C_INTF) )
    return;
i2cClearBit(I2C_INTF);

if( i2cStatBit(I2C_ARBL) )
    {
    rs232WriteByte('A');
    i2cClearBit(I2C_ARBL);
    if( i2cStatBit(I2C_IAAS) )
        i2cProcStart();
    }
else if( i2cStatBit(I2C_IAAS) )
    i2cProcStart();
else
    i2cProcData();
}

void i2cProcStart(void)
{
uint8_t junk;

rs232WriteByte('S');
if( i2cStatBit(I2C_SRW) )
    { // read by master
    REG8(0x40066002) |= 0x10;
    REG8(0x40066004) = 1;
    i2cData = 1;
    rs232WriteByte('R');
    rs232WriteByte('\r');
    rs232WriteByte('\n');
    }
else
    { // write from master
    REG8(0x40066002) &= ~0x10;
    junk = REG8(0x40066004);
    rs232WriteByte('W');
    rs232WriteHex(junk);
    rs232WriteByte('\r');
    rs232WriteByte('\n');
    }
}

void i2cProcData(void)
{
volatile uint8_t data;
if( REG8(0x40066002) & 0x10)
    { // Tx
    if( i2cStatBit(I2C_RXAK) == 0)
        { // Ack
        REG8(0x40066004) = i2cData++;
        }
    else
        { // Nack
        REG8(0x40066002) &= ~0x10;
        data = REG8(0x40066004);    // Clear out junk
        rs232WriteByte('N');
        rs232WriteHex(data);
        rs232WriteByte('\r');
        rs232WriteByte('\n');
        }
    }
else
    { // Rx
    data = REG8(0x40066004);
    rs232WriteByte('D');
    rs232WriteHex(data);
    rs232WriteByte(' ');
    }
}


void ARMmain(void)
{
// Disable watchdog.
REG16(0x40052002) = 0x20c5;
REG16(0x40052002) = 0x28d9;
REG8(0x40052001) = 0;
REG16(0x40052004) = 0xffff;
REG8(0x40052000) = 0;

pitSetup();
rs232Setup();
i2cSetup();

while(1)
    {
    procRS232();
    i2cProc();
    }
}

