NXP ARM Firmware
================

## File listing

### ``lpc111x.c``, ``lpc111x.h``
Bare-metal firmware stub for ``LPC111x`` family


### ``lpc1112.ld``
Linker script for ``LPC1112`` microcontroller chip


### ``lpc11u24-6kb.ld``, ``lpc11u24-8kb.ld``
Linker scripts for ``LPC11Uxx`` microcontroller chips


### ``lpc1112``
Subdirectory containing code samples for the ``LPC1112``.
The following are referenced from my [website article][tech-lpc1112] on
*NXP* *LPC1112* *I/O* *programming*:

* **``gpio.c``** - GPIO demonstration
* **``i2c.c``** - I2C master demonstration
* **``uart.c``** - RS232 demonstration
* **``calcBaud.py``** - Calculation utility for UART parameters
* **``makefile``** - Makefile (gmake) for building the above samples


### ``lpc11u24``
Subdirectory containing code samples for the ``LPC11Uxx``:

* **``usb.c``** - USB device firmware
* **``makefile``** - Makefile (gmake) for building the above samples


### ``ke02z.c``
Bare-metal firmware stub for the Kinetis KE02 and KE04


### ``ke02z8.ld``
Linker script for Kinetis KE02 microcontroller chip


### ``ke04z64.ld``
Linker script for Kinetis KE04 microcontroller chip


### ``ke02z64``
Subdirectory containing code samples for the Kinetis KE02:

* **``clocktest.c``** - GPIO toggling to measure clock speed
* **``i2c.c``** - I2C master demonstration
* **``serial.c``** - RS232 demonstration
* **``kinetis-i2c-params.py``** - Calculation utility for I2C parameters
* **``kinetis-rs232-params.py``** - Calculation utility for UART parameters
* **``openocd.sh``** - Script to run OpenOCD for the Olimex programmer
* **``makefile``** - Makefile (gmake) for building the above samples


### ``ke04z8``
Subdirectory containing code samples for the Kinetis KE04:

* **``clocktest.c``** - GPIO toggling to measure clock speed
* **``i2c-rs232-master.c``** - RS232-controlled I2C master
* **``openocd.sh``** - Script to run OpenOCD for the Olimex programmer
* **``makefile``** - Makefile (gmake) for building the above samples


[tech-lpc1112]: http://www.remy.org.uk/tech.php?tech=1545955200