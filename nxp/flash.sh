#!/usr/bin/expect -f

set binary [lindex $argv 0]

spawn telnet localhost 4444
expect "\r> "
send "program $binary verify reset exit\r"
expect eof

