/*
   lpc111x.h - Bootstrap for NXP LPC111x Cortex-M0 (header)
   (C) Remy Horton, 2018
   SPDX-License-Identifier: BSD-3-Clause
*/

#define REG(r) (*((volatile uint32_t *)r))

#define REG_ISER 0xe000e100  // Interrupt set enable
#define REG_ICER 0xe000e180  // Interrupt clear enable
#define REG_ISPR 0xe000e200  // Interrupt set pending
#define REG_ICPR 0xe000e280  // Interrupt clear pending

void ARMmain(void);
void ARMtrap(void);
void ARMtimeout(void);
void ARMirq(uint32_t);
