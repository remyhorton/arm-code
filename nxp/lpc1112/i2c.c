/*
   i2c.c - I2C demo
   (C) Remy Horton, 2018
   SPDX-License-Identifier: BSD-3-Clause

   The device used for testing is a TCA9535 I/O expander with slave
   address 0x40 and the two bytes written (0x06 0x00) sets the pins
   on Port 0 to output. The demo does not do reading.
*/

#include <inttypes.h>
#include <lpc111x.h>

#define REG_I2C_CTRLSET REG(0x40000000)
#define REG_I2C_CTRLCLR	REG(0x40000018)
#define REG_I2C_STATUS	REG(0x40000004)
#define REG_I2C_DATA	REG(0x40000008)
#define REG_I2C_DATABUF	REG(0x4000002c)
#define REG_I2C_ADDR0	REG(0x4000000c)
#define REG_I2C_ADDR1	REG(0x40000020)
#define REG_I2C_ADDR2	REG(0x40000024)
#define REG_I2C_ADDR3	REG(0x40000028)
#define REG_I2C_DUTYHI	REG(0x40000010)
#define REG_I2C_DUTYLO	REG(0x40000014)
#define REG_I2C_MASK0	REG(0x40000030)
#define REG_I2C_MASK1	REG(0x40000034)
#define REG_I2C_MASK2	REG(0x40000038)
#define REG_I2C_MASK3	REG(0x4000003c)
#define REG_I2C_MONITOR REG(0x4000001c)

#define I2C_AA		0x04
#define I2C_INT		0x08
#define I2C_STOP	0x10
#define I2C_START	0x20
#define I2C_EN		0x40

static void panic(void)
{
while(1);
}

void i2cStart(void)
{
REG_I2C_CTRLSET = I2C_START;
}

void i2cWait(void)
{
while( ! (REG_I2C_CTRLSET & I2C_INT) );
}

void i2cLoadData(const uint32_t data)
{
REG_I2C_DATA = data;
REG_I2C_CTRLCLR = I2C_INT | I2C_START;
}

void i2cStop(void)
{
REG_I2C_CTRLCLR = I2C_STOP;
}


void ARMmain(void)
{
REG(0x40048080) |= 1 << 16; // Enable IOCON clock (a real gotcha this one..)

REG(0x40044030) = 0x01;     // PIO0_4: SCL
REG(0x40044034) = 0x01;     // PIO0_5: SDA
REG(0x40048080) |= 1 << 5;  // Enable I2C clock
REG(0x40048004) |= 0x02;    // De-assert I2C reset

/* I2C is clocked through SYSAHBCLKDIV rather than having its own 
   frequency divider, so the reset default for I2C_PCLK is 12MHz.
   Duty cycles for I2C are covered in UM10398 Page 246.
   12MHz / (60+60) = 100kHz (see UM10398 P246) */
REG_I2C_DUTYHI = 60;
REG_I2C_DUTYLO = 60;

REG_I2C_CTRLSET = I2C_EN;   // Enable I2C
REG_I2C_CTRLCLR = I2C_INT;  // Clear interrupt flag

// Enable I2C interrupt handler (not used in this demo)
// REG(REG_ISER) |= 1lu << 15; 


// I2C Transaction: Start, addr 0x40, reg 0x06, data 0x00, stop.
// Only happy path is implemented, as this is a bare-bones demo
i2cStart();
i2cWait();
if( REG_I2C_STATUS != 0x08)
    panic();

i2cLoadData(0x40);
i2cWait();
if( REG_I2C_STATUS != 0x18)
    panic();

i2cLoadData(0x06);
i2cWait();
if( REG_I2C_STATUS != 0x28)
    panic();

i2cLoadData(0x00);
i2cWait();
if( REG_I2C_STATUS != 0x28)
    panic();

i2cStop();

while(1);
}

