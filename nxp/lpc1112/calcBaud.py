#!/usr/bin/env python

import sys

def calcParams():
    setPairs = set()
    for numer in range(1,16):
        for denom in range(1,16):
            setPairs.add( (numer,denom) )
    listDivs = []
    listValues = []
    for numer,denom in sorted(list(setPairs)):
        value = 1.0 + float(numer) / float(denom)
        if value <= 1.0 or value >= 1.9:
            continue
        if value in listValues:
            continue
        listValues.append( value )
        listDivs.append( (value,numer,denom) )
    return listDivs

def calcNewFRDL(Hz, Baud, FRest):
    DL = int(Hz / (16 * Baud * FRest))
    FR = Hz / (16.0 * Baud * DL)
    return (FR,DL)

def getNearestParams(listParams, FR):
    bestParam = listParams[0]
    bestDiff = abs(FR - bestParam[0])
    for param in listParams[1:]:
        diff = abs(FR - param[0])
        if diff < bestDiff:
            bestDiff = diff
            bestParam = param
    return bestDiff,bestParam

def calcActualBaud(Clk, DL, DivAddVal, MulVal):
    return Clk / (16.0*DL*(1.0 + float(DivAddVal) / float(MulVal)))

if len(sys.argv) != 2:
    print "Usage: {0} <baud>".format(sys.argv[0])
    sys.exit()

clockrate = 12000000
baudrate = int(sys.argv[1])

listParams = calcParams()

listCandidates = set()
for (FRinit,_,_) in listParams:
    FR,DL = calcNewFRDL(clockrate,baudrate,FRinit)
    diff,(FRtarget,num,den) = getNearestParams(listParams,FR)
    actual = calcActualBaud(clockrate,DL,num,den)
    error = abs(actual - float(baudrate))
    listCandidates.add( (int(error),DL,num,den,FRtarget) )

listSorted = sorted(listCandidates,key=lambda x : x[0])

print "Error  DLM DLL DIVADDVAL MULVAL  FRest"
print "------ --- --- --------- ------  -------------"
for err,DL,div,mul,FR in listSorted:
    print "{0:<6} {1:<3} {2:<3} {3:<9} {4:<6}  {5}".format(
            err,DL >> 8, DL & 0xff, div, mul, FR)

sys.exit()
_,_,DL,(_,numer,denom) = listSorted[0]
print "    Clock: 12MHz"
print "     BAUD:",baudrate,calcActualBaud(12000000,DL,numer,denom)
print "       DL:",hex(DL)
print "DIVADDVAL:",hex(numer)
print "   MULVAL:",hex(denom)

