/*
   uart.c - RS232 demo for NXP LPC1112
   (C) Remy Horton, 2018
   SPDX-License-Identifier: BSD-3-Clause
*/
#include <inttypes.h>
#include <lpc111x.h>

void ARMmain(void)
{
uint32_t valByte;

/* Pin and clock setup */
REG(0x40048080) |= 1 << 16;   // Enable IOCON clock
REG(0x400440a4) = 0x01;       // PIO1_6: Set to UART Rx
REG(0x400440a8) = 0x01;       // PIO1_7: Set to UART Tx
REG(0x40048080) |= 1 << 12;   // Bit 12: Enable UART clock
REG(0x40048098) = 1;          // UART_PCLK clock divider

/* UART setup
 * Baud = 12M / 16 * (256*DLM + DLL) * (1 + DivAddVal/MulVal)
 *      = 12M / 16 * (          52 ) * (1 +         1/2     )
 *      = 9615
 */
REG(0x4000800c) = 0x03;       // 8 data bits, 1 stop bit, odd parity
REG(0x4000800c) |= 1 << 7;    // DLAB=1
REG(0x40008000) = 52;         // DLL
REG(0x40008004) = 0;          // DLM
REG(0x4000800c) &= ~(1 << 7); // DLAB=0
REG(0x40008028) = 0x21;       // [0:3]=DIVADDVAL=1 [4:7]=MULVAL=2
REG(0x40008008) = 0x07;       // FIFO Enable

/* Write some data */
REG(0x40008000) = 'R';
REG(0x40008000) = 'e';
REG(0x40008000) = 'm';
REG(0x40008000) = 'y';
REG(0x40008000) = '\r';
REG(0x40008000) = '\n';

/* Echo incoming data */
while(1)
    {
    if( REG(0x40008014) & 0x01 )
        {
        valByte = REG(0x40008000);
        REG(0x40008000) = valByte;
        }
    }
}

