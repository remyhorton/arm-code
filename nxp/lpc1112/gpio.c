/*
   gpio.c - GPIO demo for NXP LPC1112
   (C) Remy Horton, 2018
   SPDX-License-Identifier: BSD-3-Clause

   Physical pins 18 & 19 should have LEDs connected, and pin 1 a push-to-close
   button that pulls the pin hi. Pressing the button toggles which LED is lit.
*/
#include <inttypes.h>
#include <lpc111x.h>

void ARMmain(void)
{
REG(0x40044010) = 0xc0; // PIO0_1: GPIO, floating
REG(0x4004401c) = 0xc0; // PIO0_2: GPIO, floating
REG(0x40044060) = 0xc8; // PIO0_8: GPIO, pull-down
REG(0x50008000) = 0x06; // Port 0: Bits 1 & 2 output
REG(0x50003ffc) = 0x02; // Port 0: Set bit 1 hi (masking disabled)

REG(0x50008004) &= ~0x100; // Port 0: Bit 8 edge-sensitive
REG(0x5000800c) = 0x100;   // Port 0: Bit 8 rising/high triggered
REG(0x50008010) = 0x100;   // Port 0: Bit 8 enable interrupt

REG(REG_ISER) = 1llu << 31; // Enable IRQ for Port 0

while(1);
}

uint32_t toggle = 0;

void ARMirq(uint32_t value)
{
if(value != 31)
    return;
if(toggle)
    {
    toggle = 0;
    REG(0x50003018) = 0x02;
    }
else
    {
    REG(0x50003018) = 0x04;
    toggle = 1;
    }
REG(0x5000801c) = 0x100; // Clear interrupt
}
